<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>#SalveSuaPele</title>
    <meta name="description" content="Tudo sobre os cuidados La Roche-Posay, o especialista em pele sensível, incluindo cuidados de rosto, maquiagem e muito mais. Aconselhamento especializado para cada tipo de pele">
    <meta name="viewport" content="width=device-width">

    <link media="screen,print" href="/resources/import/config.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/structure.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/objets.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/animate.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/stylesLocalPT.css" rel="stylesheet" type="text/css">

    <!-- FAVICON / iOS ICONS -->
    <link rel="shortcut icon" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/favicon.ico">
    <link rel="apple-touch-icon" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-ipad-retina.png">

    <script src="/resources/import/AOXJsManager.plugins.min.js"></script>
    <script src="/resources/import/AOXJsManager.min.js"></script>
    <script src="/resources/import/functions_lrp-skinchecker_3.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.5/iframeResizer.contentWindow.min.js"></script>

</head>

<body id="bodyContainerID" class="pg_landing v2 article_v4_skincheckerv3_landing pt portalv3">

    <nav class="nav-mob clearfix"></nav>
    <main>
        <section class="obj_banner var_landing" id="top">
            <div class="ctn_banner">
              <div>
                  <style>
                  .str_popin.default {
                      top: 100px !important;
                  }
                  .obj_banner.var_landing .ctn_banner h2 {
                    font-size: 3.1em;
                  }

                  .logo-sbd {
                    width: 100px;
                    margin-top: 20px;
                  }

                  @media screen and (max-width: 767px) {
                      .obj_banner.var_landing .ctn_banner h2 {
                        font-size: 1.3em;
                      }

                      .logo-sbd {
                        margin-top: 10px;
                        width: 50px;
                      }


                    }
                  </style>
                  <h3><img src="/resources/SkinChecker3/images/logo-sbd.png" class="logo-sbd" alt="SBD" /></h3>
                  <h2>Observe e cuide da pele de quem você gosta.<br> Seja um SkinChecker</h2>
                  <h3>Cheque suas pintas e aproveite o sol com segurança</h3>
                  <p class="ctn_btn">
                    <span class="obj_btn btn_video" href="javascript:void(0);" onclick="javascript:AOXJsManager.getNewPopin({idPopin: 'popinVideo', idContent: 'popinVideoHero'}).open();"
                      data-track="click"
                      data-track-action="Click"
                      data-track-category="SejaUmSkinChecker"
                      data-track-label="Play Video">Ver vídeo</span>
                  </p>
                  <div id="popinVideoHero" class="ctn_popin-zone">
                    <iframe width="899" height="506" src="https://www.youtube.com/embed/ptoLpnkPNS4" frameborder="0" allowfullscreen></iframe>
                  </div>
              </div>
            </div>
        </section>
        <nav class="obj_nav var_lp">
            <ul class="main_menu_mob">
                <li><a class="scrollTo" data-target="#top">Início</a></li>
                <li><a class="scrollTo" data-target="#spots">Cheque suas pintas</a></li>
            </ul>
        </nav>
        <section class="obj_bloc-title" id="spots">
          <div class="ctn_bloc">
            <h1>Cheque suas pintas</h1>
          </div>
        </section>
        <section class="obj_bloc-dog test">
            <h2>90% dos casos de câncer da pele podem ser curados se detectados a tempo*.</h2>
            <p>Descubra onde procurar pintas suspeitas.</p>
            <div class="obj_dog">
              <img src="/resources/SkinChecker3/images/v_dog.jpg"/>
                <ul class="obj_legendes">
                  <li class="obj_legend var_scalp"><span class="t_title">Couro cabeludo</span>
                    <div class="ctn_desc" style="left: auto; right: 44px;"><span class="t_desc">Utilize um secador para verificar todas as zonas do couro cabeludo. Para a parte de trás da cabeça, utilize espelhos para conseguir ver corretamente. Também pode pedir a um amigo ou familiar para o/a ajudar.</span>
                      <ul class="ctn_pictos">
                          <li class="obj_picto-legend var_hairdryer"></li>
                          <li class="obj_picto-legend var_handmirror"></li>
                      </ul>
                    </div>
                  </li>
                  <li class="obj_legend var_back"><span class="t_title">Costas</span>
                      <div class="ctn_desc" style="left: 44px;"><span class="t_desc">Coloque-se em frente a um espelho de corpo inteiro, utilize um espelho de mão para verificar a sua nuca, ombros, parte superior e inferior das costas.&nbsp;Verifique também as nádegas.</span>
                          <ul class="ctn_pictos">
                              <li class="obj_picto-legend var_fulllengthmirror"></li>
                              <li class="obj_picto-legend var_handmirror"></li>
                          </ul>
                      </div>
                  </li>
                  <li class="obj_legend var_last" style="top: -35x; left: -325px;"><span class="t_title">Por último,&nbsp;<br>mas não menos<br> importante</span>
                      <div class="ctn_desc" style="left: auto; right: 44px;"><span class="t_desc">Utilize um espelho para verificar o espaço entre as nádegas.</span>
                          <ul class="ctn_pictos">
                              <li class="obj_picto-legend var_handmirror"></li>
                          </ul>
                      </div>
                  </li>
                <li class="obj_legend var_legs" style="top: 120px;"><span class="t_title">Pernas</span>
                    <div class="ctn_desc" style="left: auto; right: 44px;">
                      <span class="t_desc">Sente-se e estique as pernas uma ao lado da outra, descansando os pés sobre um banco.&nbsp;Verifique os lados, a parte da frente e de trás das suas pernas, utilizando um espelho para conseguir ver corretamente.&nbsp;Verifique também as coxas, joelhos, barrigas das pernas e tornozelos.&nbsp;Não se esqueça de verificar os pés, incluindo os dedos, e por baixo das unhas.&nbsp;Por fim, verifique a planta&nbsp;dos pés e os calcanhares.</span>
                        <ul
                            class="ctn_pictos">
                            <li class="obj_picto-legend var_stool"></li>
                            <li class="obj_picto-legend var_handmirror"></li>
                            </ul>
                    </div>
                </li>
                <li class="obj_legend var_torso"><span class="t_title">Tronco</span>
                    <div class="ctn_desc" style="left: 50%; top: 32px; transform: translateX(-50%);"><span class="t_desc">Verifique o pescoço e o peito. As mulheres devem verificar por baixo do peito.</span></div>
                </li>
                <li class="obj_legend var_face"><span class="t_title">Rosto</span>
                    <div class="ctn_desc" style="left: 44px;"><span class="t_desc">Preste muita atenção a todo o seu rosto, especialmente ao nariz e narinas, lábios, interior da boca e ambos os lados das orelhas.</span></div>
                </li>
                <li class="obj_legend var_arms"><span class="t_title">Braços</span>
                    <div class="ctn_desc" style="left: 44px;"><span class="t_desc">De frente para um espelho de corpo inteiro, comece pelos cotovelos. Depois, verifique os lados dos braços e antebraços.</span>
                        <ul class="ctn_pictos">
                            <li class="obj_picto-legend var_fulllengthmirror"></li>
                        </ul>
                    </div>
                </li>
                <li class="obj_legend var_hands"><span class="t_title">Mãos</span>
                    <div class="ctn_desc" style="left: 44px;"><span class="t_desc">Verifique ambas as mãos, palmas e costas, entre os dedos e por baixo das unhas.</span></div>
                </li>
                </ul>
            </div>
            <div class="obj_dog mob">
              <img src="/resources/SkinChecker3/images/v_dog.jpg">
                <div class="obj_carrousel dog">
                    <div class="masque">
                        <div class="items clearfix" style="transform: translate(0px, 0px);">
                            <div class="item hidden">
                                <div class="obj_pastille var_scalp var_left">Couro cabeludo</div>
                                <div class="obj_pastille var_face var_right">Rosto</div>
                                <ul>
                                    <li><span class="t_name">Rosto:</span><span class="t_desc">Preste muita atenção a todo o seu rosto, especialmente ao nariz e narinas, lábios, interior da boca e ambos os lados das orelhas.</span></li>
                                    <li><span class="t_name">Couro cabeludo:</span><span class="t_desc">Utilize um secador para verificar todas as zonas do couro cabeludo. Para a parte de trás da cabeça, utilize espelhos para conseguir ver corretamente. Também pode pedir a um amigo ou familiar para o/a ajudar.</span>
                                        <ul
                                            class="ctn_pictos">
                                            <li class="obj_picto-legend var_hairdryer"></li>
                                            <li class="obj_picto-legend var_handmirror"></li>
                                </ul>
                                </li>
                                </ul>
                            </div>
                            <div class="item hidden">
                                <div class="obj_pastille var_arms var_right">Braços</div>
                                <div class="obj_pastille var_hands var_right">Mãos</div>
                                <ul>
                                    <li><span class="t_name">Braços:</span><span class="t_desc">De frente para um espelho de corpo inteiro, comece pelos cotovelos. Depois, verifique os lados dos braços e antebraços.</span>
                                        <ul class="ctn_pictos">
                                            <li class="obj_picto-legend var_fulllengthmirror"></li>
                                        </ul>
                                    </li>
                                    <li><span class="t_name">Mãos:</span><span class="t_desc">Verifique ambas as mãos, palmas e costas, entre os dedos e por baixo das unhas.</span></li>
                                </ul>
                            </div>
                            <div class="item hidden">
                                <div class="obj_pastille var_back var_right">Costas</div>
                                <div class="obj_pastille var_torso var_left top">Tronco</div>
                                <ul>
                                    <li><span class="t_name">Costas:</span><span class="t_desc">Coloque-se em frente a um espelho de corpo inteiro, utilize um espelho de mão para verificar a sua nuca, ombros, parte superior e inferior das costas.&nbsp;Verifique também as nádegas.</span>
                                        <ul
                                            class="ctn_pictos">
                                            <li class="obj_picto-legend var_fulllengthmirror"></li>
                                            <li class="obj_picto-legend var_handmirror"></li>
                                </ul>
                                </li>
                                <li><span class="t_name">Tronco:</span><span class="t_desc">Verifique o pescoço e o peito. As mulheres devem verificar por baixo do peito.</span></li>
                                </ul>
                            </div>
                            <div class="item hidden">
                                <div class="obj_pastille var_last var_left top">Por último<br> mas não menos importante</div>
                                <div class="obj_pastille var_legs var_left top">Pernas</div>
                                <ul>
                                    <li><span class="t_name">Último:</span><span class="t_desc">Utilize um espelho para verificar o espaço entre as nádegas.</span>
                                        <ul class="ctn_pictos">
                                            <li class="obj_picto-legend var_handmirror"></li>
                                        </ul>
                                    </li>
                                    <li><span class="t_name">Pernas:</span><span class="t_desc">Sente-se e estique as pernas uma ao lado da outra, descansando os pés sobre um banco.&nbsp;Verifique os lados, a parte da frente e de trás das suas pernas, utilizando um espelho para conseguir ver corretamente.&nbsp;Verifique também as coxas, joelhos, barrigas das pernas e tornozelos.&nbsp;Não se esqueça de verificar os pés, incluindo os dedos, e por baixo das unhas.&nbsp;Por fim, verifique a planta&nbsp;dos pés e os calcanhares.</span>
                                        <ul
                                            class="ctn_pictos">
                                            <li class="obj_picto-legend var_stool"></li>
                                            <li class="obj_picto-legend var_handmirror"></li>
                                </ul>
                                </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="nav">
                        <div class="prev disabled">anterior</div>
                        <ul>
                            <li style="display: none;"><a data-target="0">1</a></li>
                            <li style="display: none;"><a data-target="1">2</a></li>
                            <li style="display: none;"><a data-target="2">3</a></li>
                            <li style="display: none;"><a data-target="3">4</a></li>
                        </ul>
                        <div class="next">seguinte</div>
                    </div>
                </div>
            </div>
            <p class="mention">*Fonte: http://www.euromelanoma.org/intl/node/25&nbsp;Epidemiological fact sheet</p>
        </section>
        <script>
            jQueryObject(document).ready(function() {
                jQueryObject('.obj_carrousel.dog').carrousel({
                    minWidth: 300
                });

                // BLOC DOG
                if (jQueryObject('.obj_bloc-dog').length) {

                    var $blocDog = jQueryObject('.obj_bloc-dog');

                    $blocDog.find('.obj_legend').each(function(index) {
                        if (
                            jQueryObject(this).hasClass('var_face') ||
                            jQueryObject(this).hasClass('var_arms') ||
                            jQueryObject(this).hasClass('var_hands') ||
                            jQueryObject(this).hasClass('var_back')
                        ) {
                            jQueryObject(this).find('.ctn_desc').css({
                                'left': 44
                            });
                        } else if (
                            jQueryObject(this).hasClass('var_torso')
                        ) {
                            jQueryObject(this).find('.ctn_desc').css({
                                'left': '50%',
                                'top': 32,
                                'transform': 'translateX(-50%)'
                            });
                        } else {
                            var widthTitle = jQueryObject(this).find('.t_title').width();
                            jQueryObject(this).find('.ctn_desc').css({
                                'left': 'auto',
                                'right': 44
                            });
                        }
                    });

                    $blocDog.find('.obj_legend').bind({
                        mouseenter: function() {
                            jQueryObject(this).css({
                                'zIndex': 10
                            }).find('.ctn_desc').css({
                                'display': 'block'
                            }).animate({
                                    'opacity': 1
                                },
                                300
                            );
                        },
                        mouseleave: function() {
                            jQueryObject(this).css({
                                'zIndex': 0
                            }).find('.ctn_desc').animate({
                                    'opacity': 0
                                },
                                0,
                                function() {
                                    jQueryObject(this).css({
                                        'display': 'none'
                                    });
                                }
                            );
                        }
                    });

                }
            });
        </script>
        <section class="obj_method" id="abcde">
            <style media="screen">
            .obj_method .ctn_abcde li .obj_bubble {
              top: -260px;
            }
            .obj_method .ctn_abcde li .obj_bubble:before {
              border-left: 13px solid transparent;
              border-right: 13px solid transparent;
              border-top: 15px solid #008fcd;
              border-bottom: none;
              bottom: -15px;
              top: auto;
              }
            </style>
            <h2>Não se esqueça de usar o método ABCDE</h2>
            <div class="ctn_btn">
              <a href="/resources/downloads/abcde-method_BR.pdf" target="_blank"
                data-track="click"
                data-track-action="Click"
                data-track-category="SejaUmSkinChecker"
                data-track-label="Download PDF">
              Faça o download do método ABCDE</a>
            </div>
            <div class="ctn_abcde">
                <ul>
                    <li>
                      <a href="/resources/downloads/abcde-method_BR.pdf" target="_blank"><span class="animation letter-a"
                        data-track="click"
                        data-track-action="Click"
                        data-track-category="SejaUmSkinChecker"
                        data-track-label="Download PDF">
                        </span></a><span>para ASSIMETRIA</span>
                        <div
                            class="obj_bubble var_a">
                            <figure><img src="/resources/SkinChecker3/images/dyn/v_bubble-a.jpg"><span class="txt_1">Benigno</span><span class="txt_2">Maligno</span></figure>
                            <article>
                                <h2>A para assimetria</h2>
                                <p>
                                  A pinta benigna não é assimétrica.
                                  Se ao desenhar uma linha reta no meio da pinta, as metades forem iguais, significa que ela é <strong>simétrica</strong>.
                                  Se as metades não forem idênticas, significa que a pinta é <strong>assimétrica</strong>, o que representa um sinal de melanoma.
                                </p>
                                  <span>Fonte: Skin Cancer Foundation</span></article>
                            </div>
                    </li>
                    <li><a href="/resources/downloads/abcde-method_BR.pdf" target="_blank"><span class="animation letter-b"
                        data-track="click"
                        data-track-action="Click"
                        data-track-category="SejaUmSkinChecker"
                        data-track-label="Download PDF">
                    </span></a><span>para BORDAS</span>
                        <div
                            class="obj_bubble var_b">
                            <figure><img src="/resources/SkinChecker3/images/dyn/v_bubble-b.jpg"><span class="txt_1">Benigno</span><span class="txt_2">Maligno</span></figure>
                            <article>
                                <h2>B para bordas</h2>
                                <p>Uma pinta benigna tem as <strong>bordas</strong> lisas e uniformes, ao contrário dos melanomas. As <strong>bordas</strong> de um melanoma em fase inicial tendem a ser irregulares. As extremidades poderão ser arredondadas ou recortadas.</p><span>Fonte: Skin Cancer Foundation</span></article>
                            </div>
                    </li>
                    <li><a href="/resources/downloads/abcde-method_BR.pdf" target="_blank"><span class="animation letter-c"
                        data-track="click"
                        data-track-action="Click"
                        data-track-category="SejaUmSkinChecker"
                        data-track-label="Download PDF">
                    </span></a><span>para COR</span>
                        <div
                            class="obj_bubble var_c">
                            <figure><img src="/resources/SkinChecker3/images/dyn/v_bubble-c.jpg"><span class="txt_1">Benigno</span><span class="txt_2">Maligno</span></figure>
                            <article>
                                <h2>C para cor</h2>
                                <p>A maioria das pintas benignas são todas de uma <strong>cor </strong>- muitas vezes, têm um único tom acastanhado. Se tiver várias <strong>cores</strong>,&nbsp;é outro sinal de alerta.&nbsp;Podem surgir vários tons de castanho,
                                    bronze ou preto.&nbsp;Um melanoma também pode ser vermelho, branco ou azul.</p><span>Fonte: Skin Cancer Foundation</span></article>
                            </div>
                    </li>
                    <li><a href="/resources/downloads/abcde-method_BR.pdf" target="_blank"><span class="animation letter-d"
                        data-track="click"
                        data-track-action="Click"
                        data-track-category="SejaUmSkinChecker"
                        data-track-label="Download PDF">
                    </span></a><span>para DIÂMETRO</span>
                        <div
                            class="obj_bubble var_d">
                            <figure><img src="/resources/SkinChecker3/images/dyn/v_bubble-d.jpg"><span class="txt_1">Benigno</span><span class="txt_2">Maligno</span></figure>
                            <article>
                                <h2>D para diâmetro</h2>
                                <p>Normalmente, as pintas benignas possuem um diâmetro pequeno em relação às malignas. É comum os melanomas possuírem um diâmetro maior do que a borracha de um lápis (6mm), mas, quando detectadas pela primeira vez podem ser menores.</p><span>Fonte: Skin Cancer Foundation</span></article>
                            </div>
                    </li>
                    <li><a href="/resources/downloads/abcde-method_BR.pdf" target="_blank"><span class="animation letter-e"
                        data-track="click"
                        data-track-action="Click"
                        data-track-category="SejaUmSkinChecker"
                        data-track-label="Download PDF">
                    </span></a><span>para EVOLUÇÃO</span>
                        <div
                            class="obj_bubble var_e">
                            <figure><img src="/resources/SkinChecker3/images/dyn/v_bubble-e.jpg"><span class="txt_1">Benigno</span><span class="txt_2">Maligno</span></figure>
                            <article>
                                <h2>E para evolução</h2>
                                <p>As pintas benignas comuns mantêm o mesmo aspecto ao longo do tempo. Tenha cautela se a pinta aumentar de tamanho ou mudar o formato. Qualquer alteração nas características ou sintomas causados (coceira, sangramento, textura e etc.) deve ser avaliada por um profissional indicado.</p><span>Fonte: Skin Cancer Foundation</span></article>
                            </div>
                    </li>
                </ul>
                </div>
                <div class="ctn_btn">

                </div>
        </section>

        <div id="popinCollectData" class="ctn_popin-zone">
            <div class="obj_skinchecker_form">
                <div class="ctn_top">
                    <h3>#SalveSuaPele</h3>
                    <h2><strong>Proteção solar ótima para si e para os que mais gosta.</strong></h2>
                    <p>Verifique, proteja-se e esteja ao sol em segurança</p>
                </div>
                <div class="ctn_bottom">
                    <form id="FormCollectData" class="obj_form var_collect_data" novalidate="novalidate">
                        <div class="radio">
                            <h4>Género*</h4>
                            <div class="ctn_radio"><input class="" type="radio" name="radio_gender" id="radio_gender_01" value="1" required="required"><label for="radio_gender_01">Masculino</label><input class="" type="radio" name="radio_gender" id="radio_gender_02" value="2">
                                <label
                                    for="radio_gender_02">Feminino</label>
                            </div>
                        </div>
                        <div class="ctn_ligne clearfix">
                            <div class="text"><input type="text" id="InputFirstName" name="UserProperty.Skinchecker_FirstName" placeholder="Nome" value="" title="" required="required"></div>
                            <div class="text"><input type="email" id="InputEmail" name="FrontOfficeUser_Mail" value="" placeholder="Email" required="required"></div>
                        </div>
                        <div class="checkbox clearfix">
                            <div class="ctn_checkbox"><input type="checkbox" name="newsLetterSuscribe" id="newsLetterSuscribe" value="0"><label for="newsLetterSuscribe">Concordo em receber a newsletter Skin Checker da La Roche-Posay</label></div>
                        </div>
                        <div class="checkbox clearfix">
                            <div class="ctn_checkbox"><input type="checkbox" name="acceptCGU" id="acceptCGU" value="0" required="required"><label for="acceptCGU">Aceito os <u>termos de utilização*</u></label></div>
                        </div>
                        <div class="ctn_error errorMandatory errorInvalid" style="display:none;">
                            <p>Por favor preencha os campos obrigatórios</p>
                        </div>
                        <div class="ctn_error errorService" style="display:none;">
                            <p></p>
                        </div>
                        <p class="ctn_btn"><a class="obj_btn" onclick="onSubmitTemplateCollectData(this);">Junte-se a nós</a></p>
                        <p class="ctn_mentions">*Campos obrigatórios</p>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <!-- POPINS -->
    <script type="text/javascript">
        //popin's default options for AOXJsManager
        AOXJsManager.setDefaultOptions_Popin({
            left: 'center',
            top: 'center',
            mask: {
                color: '#000000',
                loadSpeed: 200,
                opacity: 0.5
            }
        });

        function openPopin(idPopin, idContent, onBeforeLoadFct, onLoadFct, onBeforeCloseFct, onCloseFct) {
            AOXJsManager.getNewPopin({
                idPopin: idPopin,
                idContent: idContent,
                onBeforeLoad: onBeforeLoadFct,
                onLoad: onLoadFct,
                onBeforeClose: onBeforeCloseFct,
                onClose: onCloseFct,
            }).open();
        }

        function popConfirmMessage(oArgsAsInput, sTitle, sMessage, onSubmitFct, onCancelFct) {
            AOXJsManager.popConfirmMessage({
                title: sTitle,
                msg: sMessage,
                onSubmitFct: onSubmitFct,
                onCancelFct: onCancelFct,
                custom: oArgsAsInput
            });
        }

        function popAlertMessage(sMessage, onClosePopinFct) {
            AOXJsManager.popAlertMessage({
                msg: sMessage,
                onClosePopinFct: onClosePopinFct
            });
        }

        jQueryObject(document).ready(function() {
            //on supprime le mode compatibilité avant AOXJsManager inclus par jToolsAOX.js
            jQueryObject('#popinMessage_compat').remove();
        });
    </script>
    <div id="popinDefault" class="str_popin default">
        <a href="javascript:void(0);" class="btn_close close"></a>
        <div class="logo-popin"></div>
        <div class="ctn_decor-popin">
            <div id="popinDefaultContent" class="ctn_popin"></div>
        </div>
    </div>
    <div id="popinVideo" class="str_popin default var_video">
        <a href="javascript:void(0);" class="btn_close close"></a>
        <div class="logo-popin"></div>
        <div class="ctn_decor-popin">
            <div id="popinVideoContent" class="ctn_popin"></div>
        </div>
    </div>
    <div id="popinMessageConfirm" style="display:none;">
        <div class="ctn_popin-confirm">
            <div class="t_confirm">
                <!-- Title-->
            </div>
            <div class="obj_form"></div>
            <p class="msg">
                <!-- Message -->
            </p>
            <div class="ctn_submit clearfix">
                <ul>
                    <li>
                        <a id="confirmValidateBtn" class="b_generique" title="">
                            <!-- Validate Label -->
                        </a>
                    </li>
                    <li>
                        <a id="confirmCancelBtn" class="retour" title="">
                            <!-- Cancel Label-->
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="popinMessageAlert" style="display:none;">
        <div class="ctn_popin popinAlert">
            <p class="msg">
                <!-- Message -->
            </p>
            <p>
                <a id="alertOkBtn" class="b_generique">
                    <!-- OK Label -->
                </a>
            </p>
        </div>
    </div>

    <script>
        jQueryObject(document).ready(function() {

            jQueryObject('body').removeClass('pg_article');

        });
    </script>
    <script>

        jQueryObject(document).ready(function() {
            pg_home_skinchecker();

            // INITIALISATION CARROUSEL PRODUITS
            jQueryObject('.obj_carrousel.products').carrousel({
                minWidth: 265
            });
        });
    </script>


    <table cellspacing="0" cellpadding="0" class="gstl_50 gssb_c" style="width: 2px; display: none; top: 3px; position: absolute; left: -1px;">
        <tbody>
            <tr>
                <td class="gssb_f"></td>
                <td class="gssb_e" style="width: 100%;"></td>
            </tr>
        </tbody>
    </table>


    <script>
    // Proxy events to parent frame

    jQueryObject(function(){
      $ = jQueryObject;
      var debug = false;

      var bubbleEventTracking = function(data, engine) {
        engine = engine || "GAEvent";
        data.key = engine;
        if (debug) { console.log(data); }
        parent.postMessage(JSON.stringify(data),"*");
      };

      $('[data-track]').each(function(){
        var eventName = $(this).data('track');
        $(this).on(eventName, function(event){

          var data = {
            category: $(this).data('track-category'),
            action: $(this).data('track-action') || eventName,
            label: $(this).data('track-label')
          };
          bubbleEventTracking(data);
        });
      });

    });

    </script>
</body>

</html>
