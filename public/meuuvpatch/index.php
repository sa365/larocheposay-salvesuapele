<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>#SalveSuaPele</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Tudo sobre os cuidados La Roche-Posay, o especialista em pele sensível, incluindo cuidados de rosto, maquiagem e muito mais. Aconselhamento especializado para cada tipo de pele">

    <link media="screen,print" href="/resources/import/config.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/structure.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/objets.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/animate.css" rel="stylesheet" type="text/css">
    <link media="screen,print" href="/resources/import/stylesLocalPT.css" rel="stylesheet" type="text/css">

    <!-- FAVICON / iOS ICONS -->
    <link rel="shortcut icon" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/favicon.ico">
    <link rel="apple-touch-icon" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="http://www.laroche-posay.pt/resources/SkinChecker3/images/touch-icon-ipad-retina.png">

    <script src="/resources/import/AOXJsManager.plugins.min.js"></script>
    <script src="/resources/import/AOXJsManager.min.js"></script>
    <script src="/resources/import/functions_lrp-skinchecker_3.js"></script>

    <!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-5373VR');</script>

<!-- End Google Tag Manager -->

</head>

<body id="bodyContainerID" class="pg_landing v2 article_v4_skincheckerv3_landing pt portalv3">
  <!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5373VR"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- End Google Tag Manager (noscript) -->
    <nav class="nav-mob clearfix"></nav>
    <main>
        <section class="obj_banner var_landing" id="top">
            <div class="ctn_banner">
                <div class="ctn_banner-inner">
                    <style media="screen">
                    .obj_banner.var_landing .ctn_banner h2 {
                      font-size: 3.1em;
                    }

                    .logo-sbd {
                      width: 100px;
                      margin-top: 20px;
                    }

                    @media screen and (max-width: 767px) {
                        .obj_banner.var_landing .ctn_banner h2 {
                          font-size: 1.3em;
                        }

                        .logo-sbd {
                          margin-top: 10px;
                          width: 50px;
                        }


                      }
                    </style>
                    <h3><img src="/resources/SkinChecker3/images/logo-sbd.png" class="logo-sbd" alt="SBD" /></h3>
                    <h2>Observe e cuide da pele de quem você gosta.<br> Seja um SkinChecker</h2>
                    <h3>Cheque suas pintas e aproveite o sol com segurança</h3>
                    <p class="ctn_btn">
                      <span class="obj_btn btn_video" href="javascript:void(0);" onclick="javascript:AOXJsManager.getNewPopin({idPopin: 'popinVideo', idContent: 'popinVideoHero'}).open();">Ver vídeo</span>
                    </p>
                    <div id="popinVideoHero" class="ctn_popin-zone">
                      <iframe width="899" height="506" src="https://www.youtube.com/embed/ptoLpnkPNS4" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
        <nav class="obj_nav var_lp">
            <ul class="main_menu_mob">
                <li><a class="scrollTo" data-target="#top">Início</a></li>
                <li><a class="icon_patch scrollTo" data-target="#patch"><strong>Meu UV Patch</strong></a></li>
                <li><a class="scrollTo" data-target="#safe">Aproveite o sol em segurança</a></li>
            </ul>
        </nav>

        <section>
          <div class="obj_bloc-title" id="patch">
            <div class="ctn_bloc">
              <h2>Meu UV PATCH</h2>
            </div>
          </div>
          <div class="obj_bloc-img var_patch-uv">
            <div class="ctn_bloc-img">
              <div class="ctn_txt">
                <style media="screen">
                  .feature-list {
                    margin: 40px 0;
                  }

                  .feature-list li {
                    list-style-type: disc;
                    margin: 10px 0 10px 35px;
                    padding-left: 10px;
                  }
                </style>
                <h3>Conheça o primeiro adesivo vestível e conectável que mede os raios UV e a exposição solar e te ajuda a ter um comportamento seguro em relação ao sol.</h3>
                <h4>Baixe o App Meu UV Patch e aproveite o sol em segurança com dicas personalizadas sobre comportamento seguro ao sol.</h4>
                <ul class="feature-list">
                  <li>Índice UV do local</li>
                  <li>Nível de risco de acordo com o comportamento solar</li>
                  <li>Limite diário de exposição</li>
                  <li>Indicação da reaplicação do protetor solar</li>
                  <li>Sugestão do FPS mais indicado</li>
                </ul>
                <div class="ctn_btn clearfix">
                  <a class="obj_btn var_appstore" href="https://itunes.apple.com/au/app/my-uv-patch/id1100808175?mt=8" target="_blank">Download on the App Store</a>
                  <a class="obj_btn var_goo_play" href="https://play.google.com/store/apps/details?id=com.loreal.uvpatch&amp;hl=en-AU" target="_blank">Get it on Google Play</a>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="obj_bloc-pushs double videos_patch">
          <div class="ctn_pushs clearfix">
            <article class="obj_push var_video var_white">
              <a href="javascript:void(0);" onclick="javascript:AOXJsManager.getNewPopin({idPopin: 'popinVideo', idContent: 'popinVideoPatchReveal'}).open();">
                <div class="ctn_text">
                  <h2>Meu UV Patch</h2>
                </div>
                <div class="ctn_btn">
                  <span class="obj_btn">
                    <span></span>
                  </span>
                </div>
                <img src="/resources/SkinChecker3/images/v_push-uv_patch-reveal.jpg" class="v_background">
              </a>
            </article>
            <div id="popinVideoPatchReveal" class="ctn_popin-zone">
              <iframe width="899" height="506" src="https://www.youtube.com/embed/nIQzClMXdX4?rel=0&amp;showinfo=0&amp;wmode=transparent" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            </div>
          </div>
        </section>

        <section class="obj_bloc-form_patch" style="margin-bottom: 0; padding-bottom: 50px;" id="findPatch">
          <h2>Onde encontrar o  Meu UV Patch?</h2>
          <div class="ctn_btn">
            <a class="obj_btn" style="height: 130px; line-height: 75px; padding: 30px 100px; font-size: 1.3em;" href="#">Compre o seu online</a>
          </div>
        </section>

        <section>
            <div class="obj_bloc-title" id="safe">
                <div class="ctn_bloc">
                    <h2>Aproveite o sol em segurança</h2>
                </div>
            </div>
        </section>
        <section>
            <div class="obj_bloc-tips" id="tips">
                <h3>Confira as nossas dicas de proteção</h3>
                <div class="ctn_tips">
                    <article class="obj_tip var_stayout">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-stayout.png"></div>
                        <p>Não fique exposto ao sol<br> entre 12h e 16h</p>
                    </article>
                    <article class="obj_tip var_exposure">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-exposure.png"></div>
                        <p>Crianças com menos de 3 anos<br> não devem estar sob exposição solar direta</p><a class="obj_btn" href="#">Saiba mais</a></article>
                    <article class="obj_tip var_teeshirt">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-teeshirt.png"></div>
                        <p>Use camiseta,<br> chapéu e óculos de sol</p>
                    </article>
                    <article class="obj_tip var_frequently">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-frequently.png"></div>
                        <p>Repita a aplicação de protetor solar com frequência<br>e de forma generosa</p>
                    </article>
                    <article class="obj_tip var_medication">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-medication.png"></div>
                        <p>Toma medicamentos?<br> Leia o folheto informativo<br> antes da exposição solar</p><a class="obj_btn" href="#">Saiba mais</a></article>
                    <article class="obj_tip var_pregnancy">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-pregnancy.png"></div>
                        <p>Como evitar o cloasma (pano da gravidez)?</p><a class="obj_btn" href="#">Saiba mais</a></article>
                    <article class="obj_tip var_parasol">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-parasol.png"></div>
                        <p>Sabia que os chapéus de sol<br> não protegem de todos os <br> raios UV?</p><a class="obj_btn" href="#">Saiba mais</a></article>
                    <article
                        class="obj_tip var_child">
                        <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-child.png"></div>
                        <p>Como proteger os bebês<br> da exposição solar indireta?</p><a class="obj_btn" href="#">Saiba mais</a></article>
                        <article class="obj_tip var_stayaway">
                            <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-stayaway.png"></div>
                            <p>Não faça bronzeamento artificial</p>
                        </article>
                        <article class="obj_tip var_aftersun">
                            <div class="ctn_img"><img src="/resources/SkinChecker3/images/i_tip-aftersun.png"></div>
                            <p>Como cuidar da<br> minha pele após a exposição solar?</p><a class="obj_btn" href="#">Saiba mais</a></article>
                </div>
                <div class="ctn_btn"><a class="obj_btn" href="#">Descubra mais dicas</a></div>
            </div>
        </section>

        <div id="popinCollectData" class="ctn_popin-zone">
            <div class="obj_skinchecker_form">
                <div class="ctn_top">
                    <h3>#SalveSuaPele</h3>
                    <h2><strong>Proteção solar ótima para si e para os que mais gosta.</strong></h2>
                    <p>Verifique, proteja-se e esteja ao sol em segurança</p>
                </div>
                <div class="ctn_bottom">
                    <form id="FormCollectData" class="obj_form var_collect_data" novalidate="novalidate">
                        <div class="radio">
                            <h4>Género*</h4>
                            <div class="ctn_radio"><input class="" type="radio" name="radio_gender" id="radio_gender_01" value="1" required="required"><label for="radio_gender_01">Masculino</label><input class="" type="radio" name="radio_gender" id="radio_gender_02" value="2">
                                <label
                                    for="radio_gender_02">Feminino</label>
                            </div>
                        </div>
                        <div class="ctn_ligne clearfix">
                            <div class="text"><input type="text" id="InputFirstName" name="UserProperty.Skinchecker_FirstName" placeholder="Nome" value="" title="" required="required"></div>
                            <div class="text"><input type="email" id="InputEmail" name="FrontOfficeUser_Mail" value="" placeholder="Email" required="required"></div>
                        </div>
                        <div class="checkbox clearfix">
                            <div class="ctn_checkbox"><input type="checkbox" name="newsLetterSuscribe" id="newsLetterSuscribe" value="0"><label for="newsLetterSuscribe">Concordo em receber a newsletter Skin Checker da La Roche-Posay</label></div>
                        </div>
                        <div class="checkbox clearfix">
                            <div class="ctn_checkbox"><input type="checkbox" name="acceptCGU" id="acceptCGU" value="0" required="required"><label for="acceptCGU">Aceito os <u>termos de utilização*</u></label></div>
                        </div>
                        <div class="ctn_error errorMandatory errorInvalid" style="display:none;">
                            <p>Por favor preencha os campos obrigatórios</p>
                        </div>
                        <div class="ctn_error errorService" style="display:none;">
                            <p></p>
                        </div>
                        <p class="ctn_btn"><a class="obj_btn" onclick="onSubmitTemplateCollectData(this);">Junte-se a nós</a></p>
                        <p class="ctn_mentions">*Campos obrigatórios</p>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <!-- POPINS -->
    <script type="text/javascript">
        //popin's default options for AOXJsManager
        AOXJsManager.setDefaultOptions_Popin({
            left: 'center',
            top: 'center',
            mask: {
                color: '#000000',
                loadSpeed: 200,
                opacity: 0.5
            }
        });

        function openPopin(idPopin, idContent, onBeforeLoadFct, onLoadFct, onBeforeCloseFct, onCloseFct) {
            AOXJsManager.getNewPopin({
                idPopin: idPopin,
                idContent: idContent,
                onBeforeLoad: onBeforeLoadFct,
                onLoad: onLoadFct,
                onBeforeClose: onBeforeCloseFct,
                onClose: onCloseFct,
            }).open();
        }

        function popConfirmMessage(oArgsAsInput, sTitle, sMessage, onSubmitFct, onCancelFct) {
            AOXJsManager.popConfirmMessage({
                title: sTitle,
                msg: sMessage,
                onSubmitFct: onSubmitFct,
                onCancelFct: onCancelFct,
                custom: oArgsAsInput
            });
        }

        function popAlertMessage(sMessage, onClosePopinFct) {
            AOXJsManager.popAlertMessage({
                msg: sMessage,
                onClosePopinFct: onClosePopinFct
            });
        }

        jQueryObject(document).ready(function() {
            //on supprime le mode compatibilité avant AOXJsManager inclus par jToolsAOX.js
            jQueryObject('#popinMessage_compat').remove();
        });
    </script>
    <div id="popinDefault" class="str_popin default">
        <a href="javascript:void(0);" class="btn_close close"></a>
        <div class="logo-popin"></div>
        <div class="ctn_decor-popin">
            <div id="popinDefaultContent" class="ctn_popin"></div>
        </div>
    </div>
    <div id="popinVideo" class="str_popin default var_video">
        <a href="javascript:void(0);" class="btn_close close"></a>
        <div class="logo-popin"></div>
        <div class="ctn_decor-popin">
            <div id="popinVideoContent" class="ctn_popin"></div>
        </div>
    </div>
    <div id="popinMessageConfirm" style="display:none;">
        <div class="ctn_popin-confirm">
            <div class="t_confirm">
                <!-- Title-->
            </div>
            <div class="obj_form"></div>
            <p class="msg">
                <!-- Message -->
            </p>
            <div class="ctn_submit clearfix">
                <ul>
                    <li>
                        <a id="confirmValidateBtn" class="b_generique" title="">
                            <!-- Validate Label -->
                        </a>
                    </li>
                    <li>
                        <a id="confirmCancelBtn" class="retour" title="">
                            <!-- Cancel Label-->
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="popinMessageAlert" style="display:none;">
        <div class="ctn_popin popinAlert">
            <p class="msg">
                <!-- Message -->
            </p>
            <p>
                <a id="alertOkBtn" class="b_generique">
                    <!-- OK Label -->
                </a>
            </p>
        </div>
    </div>

    <script>
        jQueryObject(document).ready(function() {

            jQueryObject('body').removeClass('pg_article');

        });
    </script>
    <script>

        jQueryObject(document).ready(function() {
            pg_home_skinchecker();

            // INITIALISATION CARROUSEL PRODUITS
            jQueryObject('.obj_carrousel.products').carrousel({
                minWidth: 265
            });
        });
    </script>


    <table cellspacing="0" cellpadding="0" class="gstl_50 gssb_c" style="width: 2px; display: none; top: 3px; position: absolute; left: -1px;">
        <tbody>
            <tr>
                <td class="gssb_f"></td>
                <td class="gssb_e" style="width: 100%;"></td>
            </tr>
        </tbody>
    </table>
</body>

</html>
