//NOTA  : 
//Signature de oOnCallBackFct(data,oParams)
//var oParams = {HbClassName:'HbMonSuperHb',param1:'ParamValue1',param2:'ParamValue2'}
//var oRequestParams = {noXsdValidation:1, noSearchUntrustedKeywords:1, resultNoXsdValidation:1}

//MKI 06/07/2016 AOXJsManager ajout param 'oRequestParams'
//MKI 22/07/2015 AOXJsManager
function callHtmlBuilderAjax(oParams, oOnCallBackFct, oRequestParams) {
	AOXJsManager.tools.service.callHtmlBuilderAjax(oParams, oOnCallBackFct, oRequestParams);
}

//MKI 23/07/2015 AOXJsManager
function backupEventHandler(obj, sEventType) {sEmptyPopinZoneId_compat
	AOXJsManager.tools.event.backupEventHandler(obj, sEventType);
}

//MKI 23/07/2015 AOXJsManager
function restoreEventHandler(obj, sEventType) {
	AOXJsManager.tools.event.restoreEventHandler(obj, sEventType);
}


//////////////////////////////////////////////////////
//MKI 20/11/2015 compatibilité POPINs avant AOXJsManager
//////////////////////////////////////////////////////
var sPopinCadreId_compat = 'popinLRPInter';
var sEmptyPopinZoneId_compat = "popinMessage_compat";
jQueryObject(document).ready(function () {
	if (jQueryObject('#' + sEmptyPopinZoneId_compat).length == 0) {
		var sHtml = "<div class='ctn_popin-zone' id ='" + sEmptyPopinZoneId_compat + "' style='display:none;'><div class='ctn_popin popinAlert'><p class='msg'></p><p><a id='alertOkBtn' class='b_generique'></a></p></div></div>";
		jQueryObject('body').append(sHtml);
	}
});

function popAlertMessage(sMessage, onClosePopinFct) {
	return AOXJsManager.popAlertMessage({
		popinId: sPopinCadreId_compat,
		containerMsgId: sEmptyPopinZoneId_compat,
		onClosePopinFct: onClosePopinFct,
		msg: sMessage
	});
}

function popConfirmMessage(oArgsAsInput, sTitle, sMessage, onSubmitFct, onCancelFct) {
	return AOXJsManager.popConfirmMessage({
		popinId: sPopinCadreId_compat,
		containerMsgId: sEmptyPopinZoneId_compat,
		custom: oArgsAsInput,
		title: sTitle,
		msg: sMessage,
		onSubmitFct: onSubmitFct,
		onCancelFct: onCancelFct
	});
}

function openPopinFunction(idPopin, idContent, onBeforeLoadFunction, onLoadFunction, onBeforeCloseFunction, onCloseFunction) {
	var options = {
		idPopin: idPopin,
		idContent: idContent,
		onBeforeLoad: onBeforeLoadFunction,
		onLoad: onLoadFunction,
		onBeforeClose: onBeforeCloseFunction,
		onClose: onCloseFunction
	};
	var popinObj = AOXJsManager.getNewPopin(options);
	popinObj.open();
}

function checkFileSize(oInputFile,nMaxLength){
	//Vérification s'il y'a un fichier de présent
	 /*Désactivé : doit rechercher fonction compatible IE !!!*/
	if(jQueryObject.browser.msie){
		try{
			var myFSO = new ActiveXObject("Scripting.FileSystemObject");
			var filepath = oInputFile.value;
			var thefile = myFSO.getFile(filepath);
			var nSize = thefile.size;
			if(nSize > nMaxLength){
				return false;
			}
		}catch(e){
			
		}
	}else{
		if(oInputFile.files.length > 0){
			//Détection nb de fichiers
			if(oInputFile.files.length > 1){
				return false;
			}else{
				//Détection taille du fichier
				if(oInputFile.files[0].size > nMaxLength){
					return false;
				}
			}
		}
	}	
	
	return true;
}
function recup_extension(fichier)
{
	if (fichier!="")
	{
		nom_fichier=fichier;
		nbchar = nom_fichier.length;
		extension = nom_fichier.substring(nbchar-4,nbchar);
		extension=extension.toLowerCase();
		return extension;
	}
}
function verif_extension(fichier)
{
	if(fichier!=""){
		ext = recup_extension(fichier);
		if(ext==".jpg"){return true;}
		else
		{
			return false;	
		}
	}
	return false;
}

function antiSpam(Name,Domain,Subject){
	location.href = 'mailto:' + Name + '@' + Domain + '?subject=' + Subject;
}

/*FBA - 27/02/2013 1.6.4.7 - Correction : 
	- conservation "#"
	- suppression param si sNewParamValue = ""
	- suppression "&" résiduels (en double, en début de QueryString, en fin de QueryString
*/
//FBA - 16/10/2012 1.5.2.2 - ajout
function findAndReplaceQueryStringParam(sHrefEnCours,sKey,sValue){
	if(typeof jQueryObject == "function"){
		sValue = jQueryObject.trim(sValue);
	}

	var sAnchor = "";
	if(sHrefEnCours.lastIndexOf("#") > 0){
		sAnchor = sHrefEnCours.substring(sHrefEnCours.lastIndexOf("#"))
		sHrefEnCours = sHrefEnCours.substring(0,sHrefEnCours.lastIndexOf("#"));
	}
	var indexDebutQS = sHrefEnCours.indexOf("?");
	if(indexDebutQS < 0 && sValue != ""){
		sHrefEnCours = sHrefEnCours + "?" + sKey + "=" + encodeURIComponent(sValue);
	}else{
		var sQueryString = sHrefEnCours.substring(indexDebutQS+1);
		var arrParams = new Array();
		arrParams = sQueryString.split("&")
		var bTrouve = false;
		for (var i=0;i<arrParams.length;i++){
			var sParamWithValue = arrParams[i];
			if (sParamWithValue.substring(0,sParamWithValue.indexOf("="))==sKey){
				if (sValue != ""){
					//Replace param
					sHrefEnCours = sHrefEnCours.replace(sParamWithValue,sKey + "=" + encodeURIComponent(sValue));
				}else{
					//Remove param
					sHrefEnCours = sHrefEnCours.replace(sParamWithValue,"");
				}
				
				bTrouve = true;
			}			
		}
		
		if(!bTrouve && sValue != ""){
			sHrefEnCours = sHrefEnCours + "&" + sKey + "=" + encodeURIComponent(sValue);
		}
	}
	
	//Clean &&
	sHrefEnCours = sHrefEnCours.replace(/&&/g,"&");
	//Clean ^&
	sHrefEnCours = sHrefEnCours.replace(/\?&/g,"?");
	//Clean &$
	sHrefEnCours = sHrefEnCours.replace(/&$/g,"");
	
	//Clean ?$
	sHrefEnCours = sHrefEnCours.replace(/\?$/g,"");
	
	return sHrefEnCours + sAnchor;
}


function getQueryStringParam(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split("=");
		if (pair[0] == variable) { return pair[1]; }
	}
	return ("");
}


// Ajout FGA - récupération de l'id client pour un control runat=server
function getClientId(tag,strid)
{
	var i=0 ;
	var pos = -1;
	var eleName = "";	 
	var tags=document.getElementsByTagName(tag);
	var count =tags.length; 
	
	for (i = 0 ;  i < count ;  i++ )
	 {
	   eleName = tags[i].id;
	   pos=eleName.indexOf ( strid ) ;
	   if(pos >= 0)  break;           
	 } 
	//if (pos>=0) alert(strid + ":" + eleName +","+pos); 
	if (pos == -1) eleName = "";
	return eleName;
 }
// Ajout FGA - récupération de l'id client pour un control runat=server

//********************************************************************
// fonction masquant / affichant les blocs de l'espace de navigation
//********************************************************************
function toggleBB(srcElement, srcArrow, strStyle) {
	var pSrcElement = document.getElementById(srcElement);
	var pSrcArrow = document.getElementById(srcArrow);
	if (pSrcElement.style.display == "none") {
		pSrcElement.style.display = "";
		pSrcArrow.className = strStyle + "-on";
	} else {
		pSrcElement.style.display = "none";
		pSrcArrow.className = strStyle + "-off";
	}
}

//********************************************************************
// fonction affichant un calendrier (à associer au fichier epoch_classes.js)
//********************************************************************
function showCalendar ( strIdElement )
{
	var strEvalCommandName = "";
	strEvalCommandName = "var calendar_" + strIdElement + ";calendar_" + strIdElement + " = new Epoch('calendar_" + strIdElement + "','popup', document.getElementById('" + strIdElement + "'), false);";
	eval ( strEvalCommandName );
	strEvalCommandName = "calendar_" + strIdElement + ".toggle();";
	eval ( strEvalCommandName );

}


///////////////////////////////////////////////////////////
// Fonction isNumber		  							 //
// Entrée : char unCaractere  							 //
// Sortie : boolean			  							 //
// 		  		   			  							 //
// Fonction retournant true si unCaractere est un nombre //
// 			faux sinon.		   			   	   	  		 //
///////////////////////////////////////////////////////////
function isNumber(unCaractere){
	var caractereCorect;
	if((unCaractere<=9)&&(unCaractere>=0)){
		caractereCorect = true;
	}
	else{
		 caractereCorect = false;
	}
	return caractereCorect;
}

//-------------------------------------------------------------

///////////////////////////////////////////////////////////
// Fonction nbSeparateurFloat						 	 //
// Entrée : string uneValeur  							 //
// Sortie : Integer			  							 //
///////////////////////////////////////////////////////////
function nbSeparateurFloat(uneValeur){
	var nbSeparateur = 0;
	var longueurValeur = uneValeur.length;
	var indice;
	var caractereLu;
	
	for(indice=0; indice<longueurValeur;indice++){
		caractereLu = uneValeur.charAt(indice);
		if((caractereLu == ".")||(caractereLu == ",")) nbSeparateur++;
	}
	return nbSeparateur;
}

///////////////////////////////////////////////////////////
// Fonction indiceSeparateur						 	 //
// Entrée : string uneValeur  							 //
// Sortie : Integer			  							 //
///////////////////////////////////////////////////////////
function indiceSeparateur(uneValeur){
	var longueurValeur = uneValeur.length;
	var indice=0;
	var caractereLu;
	
	caractereLu = uneValeur.charAt(indice);
	while((caractereLu != ".")&&(caractereLu != ",")&&(indice<longueurValeur)){
		indice++;
		caractereLu = uneValeur.charAt(indice);
	}
	if(indice>=longueurValeur) indice = "";
	return indice;
}

///////////////////////////////////////////////////////////
// Fonction indiceSeparateur						 	 //
// Entrée : string uneValeur  							 //
// Sortie : Integer			  							 //
///////////////////////////////////////////////////////////
function partieDecimale(uneValeur){
	var indiceDebut = indiceSeparateur(uneValeur);
	var indiceFin = uneValeur.length;
	var result = "";
	var indice;
	
	indiceDebut++;
	for(indice=indiceDebut; indice<indiceFin; indice++){
		result = result + uneValeur.charAt(indice);
	}
	return result;
}


///////////////////////////////////////////////////////////
// Fonction indiceSeparateur						 	 //
// Entrée : string uneValeur  							 //
// Sortie : Integer			  							 //
///////////////////////////////////////////////////////////
function partieEntiere(uneValeur){
	var indiceFin = indiceSeparateur(uneValeur);
	var result = "";
	var indice;
	
	for(indice=0; indice<indiceFin; indice++){
		caractereLu = uneValeur.charAt(indice);
		result = result + caractereLu;
	}
	return result;
}

///////////////////////////////////////////////////////////
// Fonction isFloat		  							 	 //
// Entrée : string uneValeur  							 //
// Sortie : boolean			  							 //
// 		  		   			  							 //
// Fonction retournant true si uneValeur est un reel 	 //
// 			faux sinon.		   			   	   	  		 //
///////////////////////////////////////////////////////////
function isFloat(uneValeur){
	var nbSeparateur = nbSeparateurFloat(uneValeur);
	var valeurValid = false;
	var partie1;
	var partie2;
	
	if(nbSeparateur == 1){
		partie1 = partieEntiere(uneValeur);
		partie2 = partieDecimale(uneValeur);
		if((isInteger(partie1))&&(isInteger(partie2))){
			valeurValid = true;
		}
	}
	else if(nbSeparateur == 0){
		valeurValid = isInteger(uneValeur);
	}
	return valeurValid;
}

///////////////////////////////////////////////////////////
// Fonction controlValeurReelle						 	 //
// Entrée : champ_text unChampFloat  					 //
// Sortie :			   		  							 //
///////////////////////////////////////////////////////////
function controlValeurReelle(unChampFloat){
	var messageErreur = "Veuillez saisir une valeur de type r" + String.fromCharCode(233) + "elle.";		
	var valeurChampFloat = unChampFloat.value;
	
	if(isFloat(valeurChampFloat)){
	}
	else{
		 alert(messageErreur);
		 unChampFloat.select();
	}
}

//-------------------------------------------------------------

///////////////////////////////////////////////////////////
// Fonction controlValeurChaineCaracteres			 	 //
// Entrée : champ_text unChampString  					 //
// Sortie :			   		  							 //
///////////////////////////////////////////////////////////
function controlValeurChaineCaracteres(unChampString, nombreCaracteresMax){
	var valeurChampString = unChampString.value;
	var nombreCaracteresValeurChampString = valeurChampString.length;
	var messageErreur = "Cha" + String.fromCharCode(238) + "ne limit" + String.fromCharCode(233) + "e " + String.fromCharCode(225) + " ";
	messageErreur += nombreCaracteresMax;
	messageErreur += " caract" + String.fromCharCode(232) + "res. Votre saisie en comporte ";
	messageErreur += nombreCaracteresValeurChampString;
	
	if(nombreCaracteresValeurChampString > nombreCaracteresMax){
		alert(messageErreur);
		unChampString.select();
	}
}
//////////////////////////////////////////////////////////////////////////////////
// Fonction controlAndCountMaxChars (associée à updateCharsLeft)				//
// Entrée : id (id de l'input à vérifier)										//
// Entrée : crid (id du span à mettre a jour)									//
// Entrée : max (nb char max)													//
// Sortie : Affecte au textarea (ou input) les fonctions de controle sur event	//
//////////////////////////////////////////////////////////////////////////////////
function controlAndCountMaxChars(id, crid, max)
{
	var txtarea = document.getElementById(id);
	document.getElementById(crid).innerHTML=max-txtarea.value.length;
	txtarea.onkeypress=function(){eval('updateCharsLeft("'+id+'","'+crid+'",'+max+');')};
	txtarea.onblur=function(){eval('updateCharsLeft("'+id+'","'+crid+'",'+max+');')};
	txtarea.onkeyup=function(){eval('updateCharsLeft("'+id+'","'+crid+'",'+max+');')};
	txtarea.onkeydown=function(){eval('updateCharsLeft("'+id+'","'+crid+'",'+max+');')};
}
function updateCharsLeft(id, crid, max)
{
	var txtarea = document.getElementById(id);
	var crreste = document.getElementById(crid);
	var len = txtarea.value.length;
	if(len>max)
	{
		txtarea.value=txtarea.value.substr(0,max);
	}
	len = txtarea.value.length;
	crreste.innerHTML=max-len;
}

///////////////////////////////////
// function oteEspace(uneChaine) //
///////////////////////////////////
function oteEspace(uneChaine){
	uneChaine.ParseString;
	var longValeur = uneChaine.length;
	var indice;
	var caractereLu;
	var cible = "";
	
	for(indice=0; indice<longValeur; indice++){
		caractereLu = uneChaine.charAt(indice);
		if(caractereLu != ' '){
			cible = cible + caractereLu;
		}
	}
	return cible;
}

//------------------------------------------------------------

///////////////////////////////////////////////////////////
// Fonction isInteger	  							 	 //
// Entrée : string uneValeur  							 //
// Sortie : boolean			  							 //
// 		  		   			  							 //
// Fonction retournant true si uneValeur est un réel 	 //
// 			faux sinon.		   			   	   	  		 //
///////////////////////////////////////////////////////////
function isInteger(uneValeur){
	var indice;
	var caractere;
	var longValeur;
	var valeurValid = true;
	var laValeur;
	
	indice = 0;
	laValeur = oteEspace(uneValeur);
	longValeur = laValeur.length;
	while((indice<longValeur)&&(valeurValid)){
		caractere = laValeur.charAt(indice);
		valeurValid = isNumber(caractere);
		indice++;
	}
	return valeurValid;
}


///////////////////////////////////////////////////////////
// Fonction controlValeurEntier			 	 			 //
// Entrée : champ_text unChampEntier  					 //
// Sortie :			   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function controlValeurEntier(unChampEntier){
	var messageErreur = "Veuillez saisir une valeur entière.";		
	var valeurChampEntier = unChampEntier.value;

	if(isInteger(valeurChampEntier)){
	}
	else{
		 alert(messageErreur);
		 unChampEntier.select();
	}
}

//---------------------------------------------------------------

///////////////////////////////////////////////////////////
// Fonction calculNbSeparateurDate			 	 		 //
// Entrée : string uneDate  					 		 //
// Sortie :	integer	   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function calculNbSeparateurDate(uneDate){
	var longueurDate = uneDate.length;
	var nbSeparateur = 0;
	var indice = 0;
	
	while(indice<longueurDate){
		if(uneDate.charAt(indice)== "/"){
			nbSeparateur++;
		}
		indice++;
	}
	return nbSeparateur;
	
}

///////////////////////////////////////////////////////////
// Fonction recupererJour						 	 	 //
// Entrée : string uneDate  							 //
// Sortie : boolean			  							 //
///////////////////////////////////////////////////////////
function recupererJour(uneDate){
	var leJour = "";
	var indice = 0;
	
	while(uneDate.charAt(indice) != "/"){
		leJour = leJour + uneDate.charAt(indice);
		indice++;
	}
	return leJour;
}

///////////////////////////////////////////////////////////
// Fonction recupererMois						 	 	 //
// Entrée : string uneDate  							 //
// Sortie : boolean			  							 //
///////////////////////////////////////////////////////////
function recupererMois(uneDate){
	var leMois = "";
	var nbSeparateur = 0;
	var indice = 0;
	var caractere;
	
	caractere = uneDate.charAt(indice);
	while(nbSeparateur < 2){
		if(caractere == "/"){
			nbSeparateur++;
		}
		if((caractere != "/")&&(nbSeparateur == 1)){
			leMois = leMois + caractere;
		}
		indice++;
		caractere = uneDate.charAt(indice);
	}
	return leMois;
}

///////////////////////////////////////////////////////////
// Fonction recupererAnnee						 	 	 //
// Entrée : string uneDate  							 //
// Sortie : boolean			  							 //
///////////////////////////////////////////////////////////
function recupererAnnee(uneDate){
	var annee = "";
	var longueurDate = uneDate.length;
	var nbSeparateur = 0;
	var indice = 0;
	var caractere;
	
	for(indice=0; indice<longueurDate; indice++){
		caractere = uneDate.charAt(indice);
		if(caractere == "/"){
			nbSeparateur++;
		}
		if((caractere != "/")&&(nbSeparateur == 2)){
			annee = annee + caractere;
		}
	}
	if(annee.length < 4){
	  annee = "20" + annee;
	}
	return annee;
}

/////////////////////////////////////////////////////
// Fonction isBissextile					 	 	   //
// Entrée : string uneAnnee  					   //
// Sortie : boolean			  					   //
/////////////////////////////////////////////////////
function isBissextile(uneAnnee){
	var reste400 = uneAnnee%400;
	var reste100 = uneAnnee%100;
	var reste4 = uneAnnee%4;
	var bisextile = false;
		
	if(reste100 == 0){
		if(reste400 == 0) bisextile = true;
	}
	else{
		 if(reste4 == 0) bisextile = true;
	}
	return bisextile;
}

/////////////////////////////////////////////////////
// Fonction nbJourDansMois	    	 	 	   	   //
// Entrée : string unMois, boolean anneeBisextile  //
// Sortie : Integer			  					   //
/////////////////////////////////////////////////////
function nbJourDansMois(unMois, anneeBisextile){
	var nbJour;
	
	if((unMois == 1)||(unMois == 3)||(unMois == 5)||(unMois == 7)||(unMois == 8)||(unMois == 10)||(unMois == 12)){
		nbJour = 31;
	}
	else{
		if(unMois > 12){
			nbJour = 0;
		}else{
			nbJour = 30;
		}
	}
	if(unMois == 2){
		if(anneeBisextile){
			nbJour = 29;
		}
		else{
			 nbJour = 28;
		}
	}
	return nbJour;
}

////////////////////////////////////////////////////////////////////
// Fonction correlationJourMois					 	 	   	 	  //
// Entrée : string unJour, string unMois, boolean anneeBisextile  //
// Sortie : boolean			  							   		  //
////////////////////////////////////////////////////////////////////
function correlationJourMois(unJour, unMois, anneeBisextile){
	var nbJour = nbJourDansMois(unMois, anneeBisextile);
	var correlation = false;
	if((unJour > 0)&&(unJour <= nbJour)){
		correlation = true;
	}
	return correlation;
}

/////////////////////////////////////////////////////////////
// Fonction controlValiditeDate					 	 	   //
// Entrée : string unJour, string unMois, string uneAnnee  //
// Sortie : boolean			  							   //
/////////////////////////////////////////////////////////////
function controlValiditeDate(unJour, unMois, uneAnnee){
	var dateValide = false;
	var anneeBisextile;
	
	if((isInteger(unJour))&&(isInteger(unMois))&&(isInteger(uneAnnee))){
		if(uneAnnee.length > 3){
			anneeBisextile = isBissextile(uneAnnee);
			dateValide = correlationJourMois(unJour, unMois, anneeBisextile);
		}
	}
	return dateValide;
}

///////////////////////////////////////////////////////////
// Fonction isDateComplet						 	 	 //
// Entrée : string uneDate  							 //
// Sortie : boolean			  							 //
///////////////////////////////////////////////////////////
function isDateComplet(uneDate){
	var dateCorrect = false;
	var unJour;
	var unMois;
	var uneAnnee;
	
	if(calculNbSeparateurDate(uneDate) == 2){
		unJour = recupererJour(uneDate);
		unMois = recupererMois(uneDate);
		uneAnnee = recupererAnnee(uneDate);
		
		dateCorrect = controlValiditeDate(unJour, unMois, uneAnnee);
	}
	return dateCorrect;	
}


///////////////////////////////////////////////////////////
// Fonction controlValeurDate			 	 			 //
// Entrée : champ_text unChampDate  					 //
// Sortie :			   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function controlValeurDate(unChampDate){
	var messageErreur = "Veuillez saisir correctement la date. Le format autoris" + String.fromCharCode(233) + " est le suivant : jj/mm/aaaa";
	var valeurChampDate = unChampDate.value;
	
	if((isDateComplet(valeurChampDate))||(valeurChampDate == "")){
	}
	else{
		 alert(messageErreur);
		 unChampDate.select();
	}
}
//-----------------------------------------------------------

///////////////////////////////////////////////////////////
// Fonction controlValeurMemo			 	 			 //
// Entrée : champ_memo unChampMemo  					 //
// Sortie :			   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function controlValeurMemo(unChampMemo){
	var valeurChampMemo = unChampMemo.value;
}


/////////////////////////////////////////////////////
// CONTROLE HORAIRES
/////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Fonction controlValeurHeure			 	 			 //
// Entrée : champ_text unChampHeure  					 //
// Sortie :			   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function controlValeurHeure(unChampHeure){
	var messageErreur = "Veuillez saisir une heure correcte S.V.P.";		
	var valeurChampHeure = unChampHeure.value;

	if(isInteger(valeurChampHeure)){
		if(valeurChampHeure <= 23 && valeurChampHeure >=0){
			valeurChampHeure = valeurChampHeure + "";
			if(valeurChampHeure.length == 1){
				unChampHeure.value = "0" + valeurChampHeure;
			}
		}
		else{
			alert(messageErreur);
			unChampHeure.select();
		}
	}
	else{
		 alert(messageErreur);
		 unChampHeure.select();
	}
}

///////////////////////////////////////////////////////////
// Fonction controlValeurMinute			 	 			 //
// Entrée : champ_text unChampMinute  					 //
// Sortie :			   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function controlValeurMinute(unChampMinute){
	var messageErreur = "Veuillez saisir des minutes correctes S.V.P.";		
	var valeurChampMinute = unChampMinute.value;

	if(isInteger(valeurChampMinute)){
		if(valeurChampMinute <= 59 && valeurChampMinute >=0){
		}
		else{
			alert(messageErreur);
			unChampMinute.select();
		}
	}
	else{
		 alert(messageErreur);
		 unChampMinute.select();
	}
}

/////////////////////////////////////////////////////
// CONTROLE TELEPHONE
/////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Fonction controlValeurTelephone			 	 			 //
// Entrée : champ_text unChampTelephone  					 //
// Sortie :			   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function controlValeurTelephone(unChampTelephone){
	var messageErreur = "Veuillez saisir un num" + String.fromCharCode(233) + "ro valide S.V.P.";		
	var valeurChampTelephone = unChampTelephone.value;
	var statut = true;

	if(isInteger(valeurChampTelephone)){
		controlValeurChaineCaracteres(unChampTelephone, 10);
		if(valeurChampTelephone.length < 10 && valeurChampTelephone.length > 0){
			alert(messageErreur);
			unChampTelephone.select();
			statut = false;
		}
	}
	else{
		 alert(messageErreur);
		 unChampTelephone.select();
		 statut = false;
	}
	
	return statut;
}



/////////////////////////////////////////////////////
// CONTROLE EMAIL
/////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Fonction controlValeurEmail			 	 			 //
// Entrée : champ_text unChampEmail  					 //
// Sortie :			   		  							 // 			   	   	  		 //
///////////////////////////////////////////////////////////
function controlValeurEmail(unChampEmail){
	var messageErreur = "Veuillez saisir une adresse E-mail correcte S.V.P.";
	var valeurChampEmail = unChampEmail.value;
	var stringSize;
	var indice;
	var caractereLu;
	var trouve;

	trouve = false;
	stringSize = valeurChampEmail.length;

	if(stringSize > 0){
		
		for(indice=0; indice < stringSize; indice++){
			caractereLu = valeurChampEmail.charAt(indice);
			
			if(caractereLu == '@'){
				trouve = true;
			}
			
		}

		if(trouve){
			controlValeurChaineCaracteres(unChampEmail, 255);
		}
		else{
			alert(messageErreur);
			unChampEmail.select();
		}
	}
}

//////////////////////////////////////////////////////////
// function controlValeurEmailObligatoire(unChampEmail) //
//////////////////////////////////////////////////////////
function controlValeurEmailObligatoire(unChampEmail,withAlertMsg){
	var messageErreur = "Veuillez saisir une adresse E-mail correcte S.V.P.";
	var valeurChampEmail = unChampEmail.value;
	var myRegExp = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/gi;
	
	if(! valeurChampEmail.search(myRegExp) == 0){
		if (withAlertMsg) alert(messageErreur);
		unChampEmail.select();
		return false;
	}
	return true;
}

//////////////////////////////////////////
// function InitialeMajuscule(psChaine) //
//////////////////////////////////////////
function InitialeMajuscule(psChaine){
	var sInitiale;
	var sChaine;
	var sResult = psChaine;

	if (psChaine.length>1)
	{
		sInitiale = psChaine.substring(0, 1);
		sInitiale = sInitiale.toUpperCase();
		sChaine = psChaine.substring(1, psChaine.length);
		sResult = sInitiale + sChaine;
	}
	else if (psChaine.length==1)
	{
		sResult = psChaine.toUpperCase();
	}
	return sResult;
}

function isSignedInteger(str){
	var rx = new RegExp(/^(-|\+)*[0-9]*$/); 
	return rx.test(str);
}


/* ================================
 * gestion des labelTag coté js
 * ================================*/
		
 function getJlabelTag(sCodeLabel){
	/* renvoit le labelTag associé au codeLabel passé en parametre */
	/* le tableau tabLabelTags est chargé dans pageCMS.setJTabLabelTags, appelé dans CAIPROPage.includeJs()*/
	var sResFct = sCodeLabel + " : undefined";
	
	var nNumItem;
	var oDepartement;
	var nNbItems = tabLabelTags.length;
	if(nNbItems > 0){
		for(nNumItem=0; nNumItem < nNbItems; nNumItem++)
		{
			var oLabelTag = tabLabelTags[nNumItem];		
			
			if (oLabelTag.m_strCode==sCodeLabel){
				sResFct = oLabelTag.m_strLabel;  
				nNumItem = nNbItems;
			}
		}
	}
	
	
	return sResFct; 
 } 


function inputIsEmpty(sInputId) {
	var regExpBeginning =/^\s+/;
	var regExpEnd =/\s$/;
	
	var oInput = document.getElementById(sInputId);
	var aString = oInput.value;
	
	//suppression des espaces			
	aString.replace(regExpBeginning, "").replace(regExpEnd, "");
	return (aString=="");
} 	

function radioGroupChecked(sRadioGroupName) {
	oRadioGroup = document.getElementsByName(sRadioGroupName);
	bHasOneChecked = false;
	for (i=0; i<oRadioGroup.length; i++) {
	   bHasOneChecked = oRadioGroup[i].checked;
	   if (bHasOneChecked) i=oRadioGroup.length;
	}
	return bHasOneChecked;
}

function getRadioGroupValue(sRadioGroupName) {
	oRadioGroup = document.getElementsByName(sRadioGroupName);
	bHasOneChecked = false;
	for (i=0; i<oRadioGroup.length; i++) {
	   bHasOneChecked = oRadioGroup[i].checked;
	   if (bHasOneChecked) {
			return oRadioGroup[i].value;
		}		
	}
	return "";
}

function selectDateIsValid(sSelectDayId, sSelectMonthId, sSelectYearId) {
	oDD = document.getElementById(sSelectDayId);
	oMM = document.getElementById(sSelectMonthId);
	oYY = document.getElementById(sSelectYearId);
	return ((oDD.value!="0") && (oMM.value!="0") && (oYY.value!="0"))
}

function addMsg(sMsg, sAdditionalMsg) {
	 if (sMsg!="") { sMsg = sMsg + "<br/>" ;}
	 sMsg += "- " + sAdditionalMsg;
	 return sMsg;
}

function noErrorXmlResult(oXmlResult){
  var sErrorMsg = jQueryObject(oXmlResult).find('Error').text();
  return sErrorMsg=="";
}

function getIdXmlResult(oXmlResult){
  var sIdResult = jQueryObject(oXmlResult).find('Id').text();
  return sIdResult;
}

/* ================================
 * gestion encodage values pour XML requete ajax
 * ================================*/
//MKI 23/07/2015 AOXJsManager
function encodeStringForXml(oRecordValue) {
	return AOXJsManager.tools.string.encodeStringForXml(oRecordValue);
}

//
function encodeInputValueForXml(sInputName) {
	var res = "";
	
	var oInput = document.getElementById(sInputName);
	if (oInput != undefined ) {
		if ( (oInput.type=="checkbox") || (oInput.type=="radio") ) {
			res = (oInput.checked) ? "1" : "0";	
		} else {  
			res = encodeStringForXml(oInput.value);
		}		
	} 
	
	return res;
}

/////////////////////////////////////////////
// function formateAfficheString(psString) //
/////////////////////////////////////////////
//MKI 23/07/2015 AOXJsManager
function formateAfficheString(psString)
{
	AOXJsManager.tools.string.formatStringForDisplay(psString);
}

//
function addComboBirthDateSelect(oConteneur, sId , nMinValue , nMaxValue , sValue , sDefault , sOrder) {

		var odiv = document.createElement("div");
		odiv.className =  "ctnAOX_select small";
		oConteneur.appendChild(odiv);

		var oCombo = document.createElement("select");
		odiv.appendChild(oCombo);

		oCombo.id = sId;
		oCombo.name = oCombo.id;
		
		oCombo.options[oCombo.length] = new Option(sDefault,"0");
		oCombo.options[oCombo.length-1].selected = (sValue == oCombo.options[oCombo.length-1].value);
		
		var nFirstValue=0; var nSecondValue=0; var nStep=0;
		if (sOrder=="DESC") {
			nFirstValue = nMaxValue;
			nSecondValue = nMinValue-1;
			nStep = -1;
		} else {
			nFirstValue = nMinValue;
			nSecondValue = nMaxValue+1;
			nStep = 1;
		}

		for (i=nFirstValue; i!=nSecondValue ; i+=nStep) {
			var sVal = "";
			sVal = i;
			if (i<10) sVal = "0" + sVal;
			oCombo.options[oCombo.length] = new Option(sVal,sVal);
			oCombo.options[oCombo.length-1].selected = (sValue == oCombo.options[oCombo.length-1].value);
		}	
}


//mki BEGIN - from V3 functionAOX.js 

/* ========================================
 * 					SOCIAL
 * ======================================== */

//GooglePlus
var bEnableGooglePlus = true;

//Facebook
var bEnableFacebook = true;
var autoInitFB = true;
var fbScriptURL = "//connect.facebook.net/fr_FR/all.js"


jQueryObject(document).ready(function () {
	if (bEnableGooglePlus) 
		AOXJsManager.tools.document.insertDynamicScript(document, 'google-plusone', "https://apis.google.com/js/plusone.js");
});


//
function addMetaTagFromHtmlt(opt) {
	//op : {metaContent:""" & sMetaContent & """,metaType:""" & sMetaType & """,metaCode:""" & sMetaCode & """,canAddContent:" & oMetaTagAOX.enableAdditionalContent & ",canMultipleValues:" & oMetaTagAOX.enableMultipleValues & "}
	if (typeof opt.tagName != 'undefined') {
		var oDetectedTag;
		switch (opt.tagName) {
			case 'title':
				oDetectedTag = jQueryObject(opt.tagName);
			default:
				if (opt.metaType != "undefined" && opt.metaCode != "undefined") {
					oDetectedTag = jQueryObject(opt.tagName + "[" + opt.metaType + "='" + opt.metaCode + "']");
				}
		}

		if (typeof oDetectedTag != "undefined") {
			if (oDetectedTag.length > 0) {
				if (opt.canMultipleValues) {
					switch (opt.tagName) {
						case 'title':
							jQueryObject('head').preprend('<title>' + opt.metaContent + '</title>');
						default:
							jQueryObject('head').preprend("<" + opt.tagName + " " + opt.metaType + "='" + opt.metaCode + "' content='" & opt.metaContent & "'/>");
					}
				} else {
					if (opt.canAddContent) {
						var sExistingContent = "";

						switch (opt.tagName) {
							case 'title':
								sExistingContent = oDetectedTag.html();
							default:
								sExistingContent = oDetectedTag.attr('content');
						}

						if (sExistingContent != "") {
							switch (opt.metaCode) {
								case 'fb:admins':
									sExistingContent += ", ";
								default:
									sExistingContent += " - ";
							}
						}

						sExistingContent += opt.metaContent;

						switch (opt.tagName) {
							case 'title':
								oDetectedTag.html(sExistingContent);
							default:
								oDetectedTag.attr('content', sExistingContent);
						}
					}
				}
			} else {
				switch (opt.tagName) {
					case 'title':
						jQueryObject('head').preprend('<title>' + opt.metaContent + '</title>');
					default:
						jQueryObject('head').preprend("<" + opt.tagName + " " + opt.metaType + "='" + opt.metaCode + "' content='" & opt.metaContent & "'/>");
				}

			}
		}
	}
}

//
function addJSFileFromHtmlt(id, src) {
	jQueryObject(document).ready(function () {
		var sScriptId = (id.indexOf('#') == 0) ? id : '#' + id;

		if (jQueryObject(sScriptId).length == 0) {
			var oFirstScriptAsBodyChildren = jQueryObject('html > body > script:first');
			var sScript = "<script type='text/javascript' src='" + src + "' id='" + id + "'></script>";
			if (oFirstScriptAsBodyChildren.length > 0) {
				oFirstScriptAsBodyChildren.before(sScript);
			} else {
				jQueryObject('html > body').preprend(sScript);
			}
		}
	});
}

//
function addCSSFileFromHtmlt(id, src, type) {
	jQueryObject(document).ready(function () {
		var sMediaType = typeof type != 'undefined' ? type : 'screen,print';
		var sScriptId = (id.indexOf('#') == 0) ? id : '#' + id;

		if (jQueryObject(sScriptId).length == 0) {
			var oLocalCssFile = jQueryObject('html > head > link[href*="stylesLocal"]');
			var sCss = "<link type='text/css' rel='stylesheet' href='" + src + "' media='" + sMediaType + "' />";
			if (oLocalCssFile.length > 0) {
				oLocalCssFile.before(sCss);
			} else {
				jQueryObject('html > head').append(sCss);
			}
		}
	});
}

//mki END - from V3 functionAOX.js 

if(typeof window.AsyncjToolsAOXReady == 'function'){window.AsyncjToolsAOXReady();}