/********************************/
/***** FONCTIONS GENERIQUES *****/
/********************************/
// surcharge carrousel
jQueryObject.fn.carrousel.defaults.rtl = jQueryObject('html').attr('dir') == 'rtl';


function closeCookies() {
	jQueryObject('.obj_cookies').slideUp('fast');
}

function responsiveEqualHeight( selector, options ) {
	var $window	= jQueryObject(window),
		$target = selector instanceof jQuery ? selector : jQueryObject( selector ),
		options = options || {};
		
	function onResize()
	{
		$target.height('');
		$target.equalHeight( options );
	}
	onResize();
	
	$window.on('resize', onResize );
}



/************************/
/***** pg_common()	*****/
/************************/
function pg_common_skinchecker() {

	'use-strict';
	
	jQueryObject('html').removeClass('no-js');
	
/*===== Check support animate =====*/
	if ( (AOXJsManager.getIEVersion() > 0) && (AOXJsManager.getIEVersion() <= 9) ) browserAnimOK = false;
	else browserAnimOK = true;
	
/*===== INIT SANS CONDITION DE RESOLUTION =====*/
	if ( jQueryObject('.obj_push.var_produit').length ) animateProduit();
//	if ( jQueryObject('.obj_share_testimonial').length ) initVideoAuto();
	// Init navigation principale
	MainNavigation();
	AOXJsManager.waitForWebFonts(['LocatorWeb-Light', 'LocatorWeb-Regular', 'LocatorWeb-Medium'], function() {
		responsiveEqualHeight('.obj_bloc-pushs.double_article .ctn_text');
	});

/*===== INIT TABLETTE ET DESKTOP =====*/
	if ( jQueryObject('.obj_banner .obj_share_menu').length ) openShareMailBanner();
	if ( jQueryObject('.obj_bloc-pushs .obj_share_menu').length ) openShareMailPush();

	if( !AOXJsManager.isMobile() ){
		pinMainMenu();
		// Pinned nav
		jQueryObject('header.main').pin();
	}

/*===== INIT MOBILE =====*/
	if ( AOXJsManager.isMobile() ) {
		
		if ( jQueryObject('.obj_nav-article').length ) menuMobileArticle();
		if ( jQueryObject('.obj_carrousel.self_exam').length ) initCarrouselSelfExam();
		
		menuMobileMain();
		menuMobileSprite();
		
	}

	// Carrousel produits
	AOXJsManager.waitForWebFonts(['LocatorWeb-Light', 'LocatorWeb-Regular', 'LocatorWeb-Medium'], function () {
		if (!AOXJsManager.isMobile()) {
			responsiveEqualHeight('.obj_carrousel.products .obj_push.product .ctn_infos h3');
		}
		jQueryObject('.obj_carrousel.products .obj_push.product .ctn_infos h4 a').jTruncate({
			length: 40,
			minTrail: 0,
			ellipsisText: "…",
			moreAni: "fast",
			lessAni: 2000
		});
	});

	initSummary();

}

function initCarrouselSelfExam() {
	
	jQueryObject('.obj_carrousel.self_exam').carrousel({
		loop:false,
		play:false
	});
}
/** ABCDE METHOD **/
function pg_articles() {
	
	/** Carousel SELF EXAMINATION V1 **/
	/*
	jQueryObject("#examCarousel").scrollable({
		size : 1,
		items : ".ctn_carousel article",
		next : ".nav_next",
		prev : ".nav_prev",
		touch : false,
		onBeforeSeek : function (event, i) {
			var scrollnum = this.getSize();//get number of panes
			var size = 1;//number of panes in view
			if (i == (scrollnum - size)) {
				jQueryObject(".nav_next").html("");
			} else if (i == (scrollnum - (size + 1))) {
				jQueryObject(".nav_next").html("<a href='javascript:void(0);'></a>");
			}
		}
	}).navigator();
	*/
	/** Menu mobile **/
	var h = jQueryObject(".pg_articles section.banner nav").height() - 27;
	jQueryObject(".btn_nav-mob").toggle(function() {
		jQueryObject(this).parent("nav").next(".ctn_texte").animate({opacity : "0.2"});
		jQueryObject(this).parent("nav").animate({top:"0"});
		jQueryObject(this).addClass("opened");
	}, function() {
		jQueryObject(this).parent("nav").animate({top: h * -1}, 300, function(){
			jQueryObject(this).find(".btn_nav-mob").removeClass("opened");
		});
		jQueryObject(this).parent("nav").next(".ctn_texte").animate({opacity : "1"}, {queue: false});
	});

	/** jTruncate textes pushs bottom **/
	jQueryObject('.obj_bloc-pushs.double_article article.obj_push .ctn_text p').jTruncate({
		length: 150,
		minTrail: 0,
		ellipsisText: "…",
		moreAni: "fast",
		lessAni: 2000
	});

}


/******************************/
/***** PG_HOME // LANDING *****/
/******************************/
function pg_home_skinchecker(){

	pg_common_skinchecker();
	methodHover();

}


/*******************************************/
/***** PG_STORIES // SHARE THE STORIES *****/
/*******************************************/
function pg_stories_skinchecker(){

	pg_common_skinchecker();
	AOXJsManager.getNewTemplateAOX(jQueryObject('.ctn_comment .obj_form'));
	if ( !AOXJsManager.isMobile() ) {
		jQueryObject('.ctn_pushs').masonry({
			itemSelector: '.obj_item'
		});
	}
	jQueryObject('.obj_share_masonry .obj_item .ctn_push .paragraphe a').jTruncate({
		length: 200,
		minTrail: 0,
		ellipsisText: "... »",
		moreAni: "fast",
		lessAni: 2000
	});
	
	jQueryObject('.obj_form input:not(:disabled)').customInput();

}


/*************************************/
/***** FONCTIONS PAR BLOCS		 *****/
/*************************************/

function pinMainMenu(){
	var $hauteurFenetre			=	jQueryObject(window).height()
		$hauteurHeader			=	jQueryObject('header.main').height(),
		$hauteurBanner			=	jQueryObject('.obj_banner').height(),
		$hauteurPin				=	($hauteurHeader + $hauteurBanner),
		$scrollTop 				= 	0;
	
	jQueryObject(window).on('scroll', function(){
		$scrollTop 	= 	jQueryObject(window).scrollTop();
		
		if ( $scrollTop >= $hauteurPin ){
			jQueryObject('.obj_nav.var_lp').addClass('is-pinned');
		}
		if ( $scrollTop < $hauteurPin ){
			jQueryObject('.obj_nav.var_lp').removeClass('is-pinned');
		}
	});
	
}

function animateProduit(){
	//if ( browserAnimOK ) animateObj( '.obj_push.var_produit', '.v_produit img', 'fadeInUp' );
	
	var $pushProduit = jQueryObject('.obj_push.var_produit'),
		start = $pushProduit.offset().top - jQueryObject(window).height();
	
	$pushProduit
		.parallax({
			// scrollTop
			start : start ,
			end : start + 600,
			forceTransition : true,
			// elements
			elements : {
				self : {
					//height : {from:0, to:435},
					backgroundPosition : {from:'30% 200px', to:'30% 160px'}
				},
				'.ctn_text' : {
					top : {from:-100, to:0, start:0, end:0.5},
					opacity : {from:0, to:1, start:0, end:0.5}
				},
				'.ctn_btn' : {
					opacity : {from:0, to:1, start:0.9, end:1},
					//bottom : {from:-200,to:0}
				},
				'.v_produit' : {
					bottom : {from:-400,to:0, start:0, end:0.75}	
				}
			}
		});
}

function setAnimate(blocSelector, effect, elt) {
	var eltSelector = '.' + blocSelector + ' .obj_animate';
	if (typeof elt != 'undefined')
		eltSelector += ' ' + elt;		
	var $elt = jQueryObject(eltSelector);
	if ($elt.length) {
		if (elt == 'img') {
			var oldSrc = $elt.attr("src");
			$elt.attr("src", oldSrc + "?" + new Date().getTime());
			$elt.load(function () {
				if (browserAnimOK) 
					animateObj(blocSelector, effect, elt);
			}).one();
		}
		else {
			if (browserAnimOK) 
					animateObj(blocSelector, effect, elt);
		}
	}
}

function animateObj( eltParent, elt, effect ) {
	
	var heightWindow = jQueryObject(window).height(),
	declencheur = heightWindow / 3;
	
	jQueryObject(window).resize( function() {
		heightWindow = jQueryObject(window).height();
		declencheur = heightWindow / 3;
	});
	jQueryObject(eltParent).find(elt).viewportChecker({
		classToAdd : 'is-visible animated ' + effect,
		offset : declencheur,
		repeat : false
	});
}


/************************************/
/***** AFFICHAGE FORM PATCH		*****/
/************************************/
function bindFormPatch(state){
	var stateObjet;
	
	if ( jQueryObject('.obj_step').hasClass('active') ){
		stateObjet = true;
	} else { stateObjet = false; }
	
	if (state == 'open' && stateObjet == false ){
		jQueryObject('.obj_step').eq(1).addClass('active');
		jQueryObject('.form_patch').slideDown('fast', function(){
			scrollToCible('.form_patch', '250', '');
		});
	}
	else if (state == 'close'){
		jQueryObject('.form_patch').slideUp('', function(){
			jQueryObject('#form_patch').hide('', function(){
			});
			scrollToCible('.obj_bloc-form_patch', '250', '');
		});
		jQueryObject('.obj_step').removeClass('active');
	} 
}

/************************************/
/***** SHARE MAIL LANDING		*****/
/************************************/
function openShareMailBanner(){ 
	var activeMenuMail	= 'open',
		$bannerShare		= jQueryObject('.obj_banner'),
		$menuMailBanner		= $bannerShare.find('.obj_share_menu'),
		$toggleMenuMailBanner 	= $menuMailBanner.find('.openShareMail');
		$btnNextBanner 	= $menuMailBanner.find('.btn_next');
	
	$toggleMenuMailBanner.on('click', function(){
			
		var isActiveMenuBanner = $menuMailBanner.toggleClass( activeMenuMail ).hasClass( activeMenuMail );
		
		if( isActiveMenuBanner ){
			if ( AOXJsManager.isMobile() ){
				$menuMailBanner.find('.ctn_share_menu ul').animate({'top' : '45px'});
			}
			$menuMailBanner.find('.btn_mail').animate({'margin-right' : '0'});
			$menuMailBanner.find('.step1').show().css( 'opacity' , '1');
			$menuMailBanner.find('.step2').hide().css( 'opacity' , '0');
		} 
		else {
			if ( AOXJsManager.isMobile() ){
				$menuMailBanner.find('.btn_mail').animate({'margin-right' : '-280px'});
				$menuMailBanner.find('.ctn_share_menu ul').animate({'top' : '90px'});
			}
			else {
				$menuMailBanner.find('.btn_mail').animate({'margin-right' : '-380px'});
			}
		}	
	});
	
	$btnNextBanner.on('click', function(){
		//$menuMailBanner.find('.step1').animate({'opacity' : '0'}, 500, function(){
		//	$menuMailBanner.find('.step1').hide();
		//	$menuMailBanner.find('.step2').show().animate({'opacity' : '1'}, 500);
		//});
	});
}

function openShareMailPush(){ 
	var activeMenuMail	= 'open',
		$pushShare		= jQueryObject('.obj_bloc-pushs'),
		$menuMailPush		= $pushShare.find('.obj_share_menu'),
		$toggleMenuMailBanner 	= $menuMailPush.find('.openShareMail');
		$btnNextPush 	= $menuMailPush.find('.btn_next');
	
	$toggleMenuMailBanner.on('click', function(){
			
		var isActiveMenuPush = $menuMailPush.toggleClass( activeMenuMail ).hasClass( activeMenuMail );
		
		if( isActiveMenuPush ){
			$menuMailPush.find('.btn_mail').animate({'margin-right' : '0'});
			$menuMailPush.find('.step1').show().css( 'opacity' , '1');
			$menuMailPush.find('.step2').hide().css( 'opacity' , '0');
		} 
		else {
			$menuMailPush.find('.btn_mail').animate({'margin-right' : '-380px'});
		}	
	});
	
	$btnNextPush.on('click', function(){
		//$menuMailPush.find('.step1').animate({'opacity' : '0'}, 500, function(){
		//	$menuMailPush.find('.step1').hide();
		//	$menuMailPush.find('.step2').show().animate({'opacity' : '1'}, 500);
		//});
	});
}


/************************************/
/***** VIDEO STORIES 			*****/
/************************************/
function initVideoAuto(){
	bindVideochecking('1');
}

function bindVideochecking(idVideo,idVideoYT){ 
	
	var $popinPlayerCheck 	= 	jQueryObject('.obj_share_testimonial').find('.ctn_player'),
		$conteneurIframe	=	$popinPlayerCheck.find('.ctn_iframe'); 
		$idVideoPlayer		=	'';
		
	if ($popinPlayerCheck.hasClass('open')) {
		$popinPlayerCheck.removeClass('open').animate({opacity: 0}, 500, function(){
			$conteneurIframe.find('iframe').remove();
			$popinPlayerCheck.hide();
		});
	}
	else {
		if (idVideo == 1){
			$conteneurIframe.append('<iframe width="1180" height="664" src="https://www.youtube.com/embed/'+idVideoYT+'?rel=0&amp;showinfo=0&amp;wmode=transparent&amp;autoplay=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
		}
		if (idVideo == 2){
			$conteneurIframe.append('<iframe width="1180" height="664" src="https://www.youtube.com/embed/'+idVideoYT+'?rel=0&amp;showinfo=0&amp;wmode=transparent&amp;autoplay=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
		}
		if (idVideo == 3){
			$conteneurIframe.append('<iframe width="1180" height="664" src="https://www.youtube.com/embed/'+idVideoYT+'?rel=0&amp;showinfo=0&amp;wmode=transparent&amp;autoplay=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
		}
		if (idVideo == 4){
			$conteneurIframe.append('<iframe width="1180" height="664" src="https://www.youtube.com/embed/'+idVideoYT+'?rel=0&amp;showinfo=0&amp;wmode=transparent&amp;autoplay=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
		}
		if (idVideo == 5){
			$conteneurIframe.append('<iframe width="1180" height="664" src="https://www.youtube.com/embed/'+idVideoYT+'?rel=0&amp;showinfo=0&amp;wmode=transparent&amp;autoplay=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
		}
		if (idVideo == 6){
			$conteneurIframe.append('<iframe width="1180" height="664" src="https://www.youtube.com/embed/'+idVideoYT+'?rel=0&amp;showinfo=0&amp;wmode=transparent&amp;autoplay=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
		}
		if (idVideo == 7){
			$conteneurIframe.append('<iframe width="1180" height="664" src="https://www.youtube.com/embed/'+idVideoYT+'?rel=0&amp;showinfo=0&amp;wmode=transparent&amp;autoplay=0" frameborder="0" allowfullscreen="allowfullscreen"></iframe>');
		}
		$popinPlayerCheck.addClass('open').show().animate({opacity: 1}, 500);
	}
}


/************************************/
/***** VIDEO INTRO LANDING 		*****/
/************************************/

// Load the IFrame Player API code asynchronously.
//var tag = document.createElement('script');
//tag.src = "https://www.youtube.com/player_api";
//var firstScriptTag = document.getElementsByTagName('script')[0];
//firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// Replace the 'videoIntro' element with an <iframe> and
// YouTube player after the API code downloads.
//var player;
//function onYouTubePlayerAPIReady() {
// 	var divId = 'videoIntro';
// 	var videoId = jQueryObject('#'+divId).parent().attr('data-aox-videoId');
// 	player = new YT.Player(divId, {
// 		width: '1024',
// 		height: '576',
// 		videoId: videoId,
// 		playerVars: {
// 			'showinfo': 0,
// 			'rel': 0,
// 			'wmode': 'transparent'
// 		},
// 		events: {
//             'onStateChange': onSYSYouTubePlayerStateChange
//         }		
// 	});
// }

// function onSYSYouTubePlayerStateChange(event) {
// 	switch (event.data) {
// 		case YT.PlayerState.PLAYING:
// 			if (Math.round(player.getCurrentTime()) == 0) {
// 				if (typeof onSYSYoutubePlayerStart == 'function'){
// 					onSYSYoutubePlayerStart(player.getVideoData().video_id, player.getVideoData().title);
// 				}
// 			}
// 			break;
// 	};	
// }

// function bindVideoIntroTracking(ctrl, trackingFct){ 
// 	//tracking 
// 	if (typeof ctrl != 'undefined') {
// 		if (typeof trackingFct == 'function') 
// 			trackingFct(ctrl);
// 		else if (typeof window[trackingFct] == 'function') 
// 			window[trackingFct](ctrl);
// 	}
	
// 	bindVideoIntro();
// }

// function bindVideoIntro(ctrl, trackingFct){ 
// 	var $popinPlayer = jQueryObject('.ctn_banner').find('.ctn_player');
// 	if ($popinPlayer.hasClass('open')) {
// 		$popinPlayer.removeClass('open').animate({opacity: 0}, 500, function(){
// 			$popinPlayer.hide();
// 			player.pauseVideo()
// 		});
// 	}
// 	else {
// 		$popinPlayer.addClass('open').show().animate({opacity: 1}, 500);
// 	}
// }


/********************************/
/***** MENU MOBILE 			*****/
/********************************/
/* MENU MOBILE HORS LANDING */
function menuMobileMain() {
	var headerSite	=	jQueryObject('header.main'),
		menuMain	=	jQueryObject('main .obj_nav.var_lp'),
		navMenuMain	=	menuMain.html();
		
		headerSite.append('<nav class="main">' + navMenuMain + '</nav>');
		jQueryObject('nav.main').prepend('<button class="btn_menu_mob">Menu</button>');
		
}

function menuMobileSprite() {
	var activeMenu	= 'open',
		$header		= jQueryObject('header.main'),
		$nav		= $header.find('nav.main'),
		$menuMob	= $nav.find('.main_menu_mob'),
		$linksMenu	= $menuMob.find('a'),
		$toggleMenu = $header.find('.btn_menu_mob');
	
	$toggleMenu.sprite().on('click', function(){
			
		var isActiveMenu = $nav.toggleClass( activeMenu ).hasClass( activeMenu );
		
		if( isActiveMenu ){
			$toggleMenu.sprite('play');
			$menuMob.slideDown("slow");
		} 
		else {
			$toggleMenu.sprite('play',{reverse:true});
			$menuMob.slideUp("slow")
		}	
	});
	
	$linksMenu.on('click', function(){
		$toggleMenu.trigger('click');
	});
}

function menuMobileArticle() {
	
	var bOpen = false,
		heightMenuArticle = jQueryObject('.obj_nav-article').find('.ctn_nav-article').outerHeight();
		
	jQueryObject('.btn_menu_article').bind('click', function(){
		if ( !bOpen ) {
			jQueryObject(this).addClass('open');
			jQueryObject(this).parents('.obj_conteneur_article').find('.obj_nav-article').addClass('open').animate(
				{ 'height' : heightMenuArticle},
				400,
				function () { bOpen = true; }
			)
		}
		else {
			jQueryObject(this).removeClass('open');
			jQueryObject(this).parents('.obj_conteneur_article').find('.obj_nav-article').animate({ 'height' : 0 }, 400, function(){
					bOpen = false;
					jQueryObject(this).removeClass('open');
				}
			);
		}
	});
}


/*********************************/
/***** NAVIGATION PRINCIPALE *****/
/*********************************/
function MainNavigation() {
	
	jQueryObject('nav.main > ul > li').each( function() {
		if ( jQueryObject(this).find('.sub-nav').length ) {
			jQueryObject(this).addClass('ss_nav');
			jQueryObject(this).children('a').attr('href','javascript:void(0)');
			jQueryObject('<span class="picto"></span>').appendTo(jQueryObject(this).children('a'));
		}
	});
	
	var o = {
		$bMain: jQueryObject('nav.main > ul > li > a'),
		$parent: jQueryObject('nav.main > ul > li > a').parent('li'),
		$subNav : jQueryObject('nav.main .sub-nav')
	}
	
	var utils = {
		timerOut: '',
		delayOut: 1000,
		timerIn: '',
		delayIn: 1000
	}

	var CONST = {
		TRANSITION_DELAY: 50
	}
	
	main();
	
	function main() {
		if (jQueryObject.support.touchstart || jQueryObject.support.gesturestart) setTouchevents();
		else setEvents();
	}

	function setTouchevents() {
		o.$bMain.on('click', ctrlTactile);
	}

	function ctrlTactile(e) {
		if (jQueryObject(e.currentTarget).next('.sub-nav').hasClass('open')) closeNav();
		else openNav(e);
	}

	function setEvents() {
		o.$bMain.on({
			mouseleave: function () { clearTimeout(utils.timerIn); },
			mouseenter: utils.delayIn > 1000 ? setTimerIn : openNav
		});

		o.$parent.on({
			mouseenter: function () { clearTimeout(utils.timerOut); },
			mouseleave: utils.delayOut > 1000 ? setTimerOut : closeNav
		});
	}

	function setTimerIn(e) {
		utils.timerIn = setTimeout(function () { openNav(e) }, utils.delayIn);
	}

	function setTimerOut(e) {
		utils.timerOut = setTimeout(closeNav, utils.delayOut);
	}

	function openNav(e) {
		var $target = jQueryObject(e.currentTarget);

		var $subNav = $target.next('.sub-nav');

		if (jQuery.support.transition){
			if ( !AOXJsManager.isMobile() ){
				$subNav.css({ height: 432 }).addClass('open');
			}
			else {
				$subNav.css({ height: getHeight($subNav) }).addClass('open');
			}
		}
		else {
			if ( !AOXJsManager.isMobile() ){
				$subNav.addClass('open').stop().animate({ height: 432 }, CONST.TRANSITION_DELAY);
			}
			else {
				$subNav.addClass('open').stop().animate({ height: getHeight($subNav) }, CONST.TRANSITION_DELAY);
			}
		}

		$subNav.prev('a').addClass('active');

	}

	function closeNav() {
		if (jQuery.support.transition){
			if ( !AOXJsManager.isMobile() ){
				o.$subNav.removeClass('open').css({ height: 0 });
			}
		}
		else {
			if ( !AOXJsManager.isMobile() ){
				o.$subNav.removeClass('open').stop().animate({ height: 0 }, CONST.TRANSITION_DELAY);
			}
		}

		o.$subNav.prev('a').removeClass('active');

	}

	function getHeight($target) {
		var h = 0;

		$target.children().each(function () {
			h += jQueryObject(this).outerHeight(true);
		});

		return h;
	}
	
	// fonction nettoyage array
	Array.prototype.unset = function( val ){
		var index = this.indexOf( val );
		if ( index > -1 ) this.splice(index,1);
	}

};


/******************************/
/***** NAVIGATION ONGLETS *****/
/******************************/
function showTabContent(tabContentId) {
	
	if (typeof console == 'object') { console.log('show : ' + tabContentId); }
	if (!AOXJsManager.isMobile()){
		jQueryObject('#' + tabContentId).parents('.ctn_product-detail').find('.str_tab-content').animate(
			{ 'opacity': 0 },
			200,
			function () {
				jQueryObject(this).hide();
				jQueryObject('#' + tabContentId).show();
				jQueryObject('#' + tabContentId).animate({ opacity: 1 }, 200);
				jQueryObject(this).children('.jQueryScroll').jScrollPane();
			}
		);
	}
}


/* ****************************** */
/* AFFICHER INGREDIENTS EN MOBILE */
/* ****************************** */
function showIngredients(tabContentId){
	
	if (AOXJsManager.isMobile()) {
		jQueryObject('#' + tabContentId).find('.ctn_ingredients').toggle();
		jQueryObject('#' + tabContentId).find('.tabs_title').toggleClass('active');
	}
	
}


/*************************/
/* CUSTOM RADIO CHECKBOX */
/*************************/

/** Customized Radio/Checkboxes **/
jQuery.fn.customInput = function () {

	var nbObjRadioCheck;

	jQueryObject(this).each(function (i) {
		if (jQueryObject(this).is('[type=checkbox],[type=radio]')) {

			var input = jQueryObject(this);

			// get the associated label using the input's id
			var label = jQueryObject('label[for="' + input.attr('id') + '"]');

			//get type, for classname suffix 
			var inputType = (input.is('[type=checkbox]')) ? 'checkbox' : 'radio';

			// wrap the input + label in a div 
			jQueryObject('<div class="custom-' + inputType + '"></div>').insertBefore(input).append(input, label);

			// find all inputs in this set using the shared name attribute
			var allInputs = jQueryObject('input[name="' + input.attr('name') + '"]');

			// necessary for browsers that don't support the :hover pseudo class on labels
			label.hover(
				function () {
					jQueryObject(this).addClass('hover');
					if (inputType == 'checkbox' && input.is(':checked')) {
						jQueryObject(this).addClass('checkedHover');
					}
				},
				function () { jQueryObject(this).removeClass('hover checkedHover'); }
			);

			//bind custom event, trigger it, bind click,focus,blur events
			input.bind('updateState', function () {

				if (input.is(':checked')) {
					if (input.is(':radio')) {
						allInputs.each(function () {
							jQueryObject('label[for=' + jQueryObject(this).attr('id') + ']').removeClass('checked');
						});
					};

					if (input.is(':checkbox') && input.closest('.custom-checkbox').siblings('[type=hidden]').length > 0) {
						input.closest('.custom-checkbox').siblings('[type=hidden]').val('1');
					};

					label.addClass('checked');

				}
				else {
					if (input.is(':checkbox') && input.closest('.custom-checkbox').siblings('[type=hidden]').length > 0) {
						input.closest('.custom-checkbox').siblings('[type=hidden]').val('0');
					};
					label.removeClass('checked checkedHover checkedFocus');
				}
			})
			.click(function () {
				jQueryObject(this).trigger('updateState');
			})
			.focus(function () {
				label.addClass('focus');
				if (inputType == 'checkbox' && input.is(':checked')) {
					jQueryObject(this).addClass('checkedFocus');
				}
			})
			.blur(function () { label.removeClass('focus checkedFocus'); });

		}
	});

};

/* ========================================
 *	FORMULAIRE SHARE / TEXT IMAGE VIDEO
 * ======================================== */
function showFormMedia(idForm){
	jQueryObject(".obj_form").animate(
		{"opacity": 0},
		200,
		function(){
			jQueryObject(".obj_form").hide();
			jQueryObject("#" + idForm).show().animate({"opacity": 1}, 200);
		}
	);
	jQueryObject('.ctn_boutons').find('a').removeClass('active');
	jQueryObject('.ctn_boutons').find('a.'+idForm).addClass('active');
}

/* ========================================
 *	$ =	OBJECT FORM METHOD
 * ======================================== */
var oFm = {
	
	// clearfield
	clearField : function(fieldTarget, defaultValue)
	{
		
		if( jQueryObject( fieldTarget ).val() == defaultValue || jQueryObject( fieldTarget ).val() == '' )
		{ 
			jQueryObject( fieldTarget )
				.val('')
				.off( 'focusout' )
				.on('focusout',function(){ restoreValue( fieldTarget, defaultValue ) });
		}
		
		function restoreValue( fieldTarget, defaultValue )
		{
			if( jQueryObject( fieldTarget ).val() == '' )
			{ 
				jQueryObject( fieldTarget )
					.val( defaultValue )
			}
		}		
	},
	
	// filter
	filter : function( target )
	{
		var $target = jQueryObject( target ),
			touchDevice = oGM.is_touch_device();
		
		oFm.removeClassError( $target );
		
		( $target.hasClass('open') ) ? closeAllFilters() : openTargetFilter();
		
		if( !$target.attr('data-init') )
		{
			$target.attr('data-init', 'true' );
			$target.parents('.obj_filter').find('li a').on('click', changeMainLink );	
					
			if( !touchDevice.bool )
			{
				$target.parents('.obj_filter')	
					.off('mouseleave')
					.on('mouseleave', closeAllFilters);
			}
		}
		
		function changeMainLink( event )
		{
			$target.parents('.obj_filter').find('p a').html( jQueryObject(event.currentTarget).html() );	
		}
		
		function openTargetFilter()
		{
			closeAllFilters();
			$target
				.addClass('open')
				.parent('p').parent('div').css({height:'auto'})
				.parent('.obj_filter')
					.css({zIndex:'990'});
		}
		
		function closeAllFilters()
		{
			jQueryObject('.obj_filter p a')
				.removeClass('open')
				.parent('p').parent('div').css({height:'28px'})
				.parent('.obj_filter').removeAttr('style');
		}
	},
	
	Select : function( toggleButton )
	{
		
		var $target = jQueryObject( toggleButton ),
			$liste = $target.parent().next('.jQueryScroll'),
			keyboardText = "",
			number = "";
		
		var utils = {
			targetH : 138,
			H : 28	
		};
		
		var touchDevice = oGM.is_touch_device();
		
		oFm.removeClassError( $target );
		
		$liste.find('a').on('click', function()
		{
			var value = jQueryObject(this).attr('data-value');
			var texte = jQueryObject(this).html();
			
			$target.html( jQueryObject(this).html() );
			
			$target.parents('.obj_select').find('input[type=hidden]').val( value );
			
			closeAllSelect();
		});
		
		if( $target.hasClass('open') )
		{
			closeAllSelect();
		}
		else
		{
			closeAllSelect();
			openSelect();
		}
		
		function closeSelect()
		{
			$liste.find( 'li' ).removeClass('current').css({display:'block'});
			$target.removeClass('open');
			$target.parent().parent().css({height: utils.H +'px'});
			$liste.css('display','none');
		}
		
		function closeAllSelect()
		{
			jQueryObject( '.obj_select' ).find( 'li' ).removeClass('current').css({display:'block'});
			jQueryObject( '.obj_select' ).css({zIndex:"1"});
			jQueryObject( '.obj_select > div > p a' ).removeClass('open');
			jQueryObject( '.obj_select > div' ).css({height:utils.H +'px'});
			jQueryObject( '.obj_select .jQueryScroll' ).css('display','none');
		}
		
		function openSelect()
		{
			$liste.css('display','block');
			$target.addClass('open');
			$target.parent().parent().css({height: utils.targetH + 'px'});
			$target.parents('.obj_select').css({zIndex:'100'});
			
			$liste.jScrollPane().data('jsp').reinitialise();
			
			keyboardText = "";
			number = "";
			
		}
		
		function controlKeyboard( e )
		{
			// entree ou tab
			if( e.which == 13 || e.which == 9)
			{
				$liste.find('li.current:eq(0) a').focus().trigger('click');
				return;
			}
			// shift
			if( e.which == 16 || e.which == 20 )
			{
				return
			}
			// control
			keyboardText += String.fromCharCode( e.which );
			number += (e.which - 96).toString();
			
			var expTxt = new RegExp("^"+keyboardText);
			var expNum = new RegExp('^'+number);
			
			$liste.find('a').each(function()
			{
				if( !expTxt.test( jQueryObject(this).html().toUpperCase() ) && !expNum.test( jQueryObject(this).html() ))
				{
					jQueryObject(this).parent()
						.removeClass('current')
						.css({display:'none'});
				}
				else
				{
					jQueryObject(this).parent().addClass('current')	
				}
			});
			
			$liste.jScrollPane().data('jsp').reinitialise();
		}
		
		if( touchDevice.bool != true && touchDevice.evt != "MSPointerEvent" )
		{
			jQueryObject('.obj_select').off('mouseleave').on('mouseleave',function()
			{
				closeSelect();
			});
		}
	},
	
	// Checkbox
	Checkbox : function( target )
	{
		
		var $target = jQueryObject( target );
		
		oFm.removeClassError( $target );

		if( $target.children('input').is(':checked') )
		{
			$target.addClass('checked');	
			//$target.next('input[type=hidden]').val('1');	
			$target.parent().find('input[type=hidden]').val('1');	
		}
		else
		{
			$target.removeClass('checked');
			//$target.next('input[type=hidden]').val('0');
			$target.parent().find('input[type=hidden]').val('0');
		}
	},
	
	updateCheckboxValue : function( event )
	{
		var $target = jQueryObject( event.currentTarget ).find('input');
		
		var s = "", v="";
		
		jQueryObject('input[type=checkbox][name='+ $target.attr('name') +']:checked').each(function()
		{
			s+= v + jQueryObject(this).val();
			v = ",";
		});
		
		jQueryObject('input[type=hidden][name=list_'+ $target.attr('name') +']').val( s );
	},
	
	/* radio */
	Radio : function( target )
	{
		var $target = jQueryObject( target );
		
		oFm.removeClassError( $target );

		if( $target.children('input').is(':checked') )
		{
			$target.parents('.ctn_radio').find('.checked').removeClass('checked');
			$target.addClass('checked');
			$target.parents('.ctn_radio').find('input[type=hidden]').val( $target.children('input[type=radio]').val() )	
		}
		else
		{
			$target.removeClass('checked');
		}
	},
	
	/* evaluation */
	initEvaluation : function()
	{
		jQueryObject('.obj_evaluation').each(function()
		{
			$this = jQueryObject(this);
			
			if( $this.data('init') == true ) return;
			
			$this.data('init', true);
			
			$this.children('label')
				.on('mouseover', function(){
					jQueryObject(this).prevAll('label').addClass('over')
					jQueryObject(this).nextAll('label').removeClass('over');
				})
				.on('mouseout',function(){
					
				})
				.on('click', function(){
					$this.children('input[type=hidden]').val(jQueryObject(this).children().val());
					
					jQueryObject(this)
						.addClass('on');
					jQueryObject(this).prevAll('label')
						.removeClass('over')
						.addClass('on');
					jQueryObject(this).nextAll('label')
						.removeClass('on');
					
				});
				
			$this.on('mouseout',function()
			{
				$this.children('label').removeClass('over');
			});
		});
	},
	
	removeClassError : function( $target )
	{
		$target
			.removeClass('error')
			.parents('p').removeClass('error');	
	},
	
	/* custom input file */
	customInputFile : function(e)
	{
		
		var $inputFile = jQueryObject(e.currentTarget).parent().find('input[type=file]'),
			$inputText = $inputFile.parent().find('input[type=text]');
		
		if( !$inputFile.data('init') )
		{
			$inputFile
				.on('change',function(){
					$inputText.val( $inputFile.val() )
				})
				.data('init', true);
		}
		
		$inputFile.click();
	}
}


/****************/
/***** QUIZ *****/
/****************/
function pg_quiz(){
	pg_common_skinchecker();
}

function launchQuiz(){
	jQueryObject('.obj_quiz .intro').animate({opacity : "0"}).hide();
	jQueryObject('.question.q_01, .obj_progressbar').show().animate({opacity : "1"});
	if ( AOXJsManager.isMobile() ) {
		jQueryObject('html, body').animate({scrollTop: jQueryObject(".obj_quiz .ctn_quiz").offset().top}, 400);
	}
}

function showAnswer(answerNb){
	jQueryObject('.question.q_0' + answerNb).animate({opacity : "0"}).hide();
	jQueryObject('.answer.q_0' + answerNb ).show().animate({opacity : "1"});
	if ( AOXJsManager.isMobile() ) {
		jQueryObject('html, body').animate({scrollTop: jQueryObject(".obj_quiz .ctn_quiz").offset().top}, 400);
	}
}

function nextQuestion(questionNb){
	jQueryObject('.answer').animate({opacity : "0"}).hide();
	jQueryObject('.question.q_0' + questionNb ).show().animate({opacity : "1"});
	jQueryObject('.obj_progressbar ul li').removeClass('active');
	jQueryObject('.obj_progressbar ul li.step_' + questionNb).addClass('active');
	if ( AOXJsManager.isMobile() ) {
		jQueryObject('html, body').animate({scrollTop: jQueryObject(".obj_quiz .ctn_quiz").offset().top}, 400);
	}
}

function endQuiz(){
	jQueryObject('.question, .answer, .obj_progressbar').animate({opacity : "0"}).hide();
	jQueryObject('.end').show().animate({opacity : "1"});;
	}


/********************************/
/***** HOVER METHOD LETTERS *****/
/********************************/
function methodHover(){
	if ( !AOXJsManager.isMobile() & !AOXJsManager.isTablet() ) {
		jQueryObject('.obj_method .ctn_abcde li .animation').each(function(){
			jQueryObject(this).hover(
				function() {
					jQueryObject(this).parent('a').nextAll('.obj_bubble').fadeIn();
				}, function() {
					jQueryObject(this).parent('a').nextAll('.obj_bubble').hide();
				}
			);
		})
	}
}







function initSummary() {

	jQuery( 'a.scrollTo' )
	.each( function( index ) {
		var $target = jQuery( this );
		var sHash = $target.attr( 'href' );
		$target.removeAttr( 'href ').attr( 'data-target', sHash );
	} )
	.bind( 'click', function( event ) {
		scrollToTarget( event );
	});

}


function scrollToTarget( event ) {

	scrollToCible( jQuery( jQuery( event.currentTarget ).attr('data-target') ) );

}


function scrollToCible( cible ) {

	var $target = jQuery( cible ),
		speed,
		origine;

	origine = jQuery( window ).scrollTop();
	speed = Math.round( Math.abs( ( $target.position().top - origine ) / 3) );
	
	var dest = $target.offset().top - 60;
	
	//PATCH LANDING freefromspots V3 SCROLL-TO & PINNED
	if ( jQueryObject('.pg_landing').length ) {
		if (!AOXJsManager.isMobile()){
			if ( $target.attr('id') == 'top' ) dest = 0;
			if ( !jQueryObject('.obj_nav.var_lp').hasClass('is-pinned') ) dest = dest - 60;
		}
		else { dest = dest +20; }
	}

	jQuery('body,html').animate(
		{ scrollTop: dest },
		speed, 
		function(){
		}
	);
}